<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Multiple security vulnerabilities were found in Imagemagick. Missing or
incomplete input sanitizing may lead to undefined behavior which can result
in denial of service (application crash) or other unspecified impact.</p>

<p>For Debian 9 stretch, these problems have been fixed in version
8:6.9.7.4+dfsg-11+deb9u12.</p>

<p>We recommend that you upgrade your imagemagick packages.</p>

<p>For the detailed security status of imagemagick please refer to
its security tracker page at:
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/imagemagick">https://security-tracker.debian.org/tracker/imagemagick</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2602.data"
# $Id: $
