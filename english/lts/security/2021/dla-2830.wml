<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>An infinite loop when --sparse is used with file shrinkage during read
access was fixed in the GNU tar archiving utility.</p>

<p>For Debian 9 stretch, this problem has been fixed in version
1.29b-1.1+deb9u1.</p>

<p>We recommend that you upgrade your tar packages.</p>

<p>For the detailed security status of tar please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/tar">https://security-tracker.debian.org/tracker/tar</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2830.data"
# $Id: $
