<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Multiple security vulnerabilities were discovered in GlusterFS, a
clustered file system. Buffer overflows and path traversal issues may
lead to information disclosure, denial-of-service or the execution of
arbitrary code.</p>

<p>For Debian 9 stretch, these problems have been fixed in version
3.8.8-1+deb9u1.</p>

<p>We recommend that you upgrade your glusterfs packages.</p>

<p>For the detailed security status of glusterfs please refer to
its security tracker page at:
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/glusterfs">https://security-tracker.debian.org/tracker/glusterfs</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2806.data"
# $Id: $
