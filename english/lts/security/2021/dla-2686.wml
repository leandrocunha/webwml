<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities were discovered in python-urllib3, a HTTP
client for Python.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-20060">CVE-2018-20060</a>

    <p>Urllib3 does not remove the Authorization HTTP header when
    following a cross-origin redirect (i.e., a redirect that differs
    in host, port, or scheme). This can allow for credentials in the
    Authorization header to be exposed to unintended hosts or
    transmitted in cleartext.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-11236">CVE-2019-11236</a>

    <p>CRLF injection is possible if the attacker controls the request
    parameter.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-11324">CVE-2019-11324</a>

    <p>Urllib3 mishandles certain cases where the desired set of CA
    certificates is different from the OS store of CA certificates,
    which results in SSL connections succeeding in situations where a
    verification failure is the correct outcome. This is related to
    use of the ssl_context, ca_certs, or ca_certs_dir argument.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-26137">CVE-2020-26137</a>

    <p>Urllib3 allows CRLF injection if the attacker controls the HTTP
    request method, as demonstrated by inserting CR and LF control
    characters in the first argument of putrequest().</p></li>

</ul>

<p>For Debian 9 stretch, these problems have been fixed in version
1.19.1-1+deb9u1.</p>

<p>We recommend that you upgrade your python-urllib3 packages.</p>

<p>For the detailed security status of python-urllib3 please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/python-urllib3">https://security-tracker.debian.org/tracker/python-urllib3</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2686.data"
# $Id: $
