<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>In Apache FOP before 2.2, files lying on the filesystem of the server
which uses FOP can be revealed to arbitrary users who send maliciously
formed SVG files. The file types that can be shown depend on the user
context in which the exploitable application is running. If the user is
root a full compromise of the server - including confidential or
sensitive files - would be possible. XXE can also be used to attack the
availability of the server via denial of service as the references
within a xml document can trivially trigger an amplification attack.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
1:1.0.dfsg2-6+deb7u1.</p>

<p>We recommend that you upgrade your fop packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-927.data"
# $Id: $
