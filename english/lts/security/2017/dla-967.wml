<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Gajim implements XEP-0146, an XMPP extension to run commands remotely
from another client. However it was found that malicious servers can
trigger commands, which could lead to leaking private conversations
from encrypted sessions. To solve this, XEP-0146 support has been
disabled by default.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
0.15.1-4.1+deb7u3.</p>

<p>We recommend that you upgrade your gajim packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-967.data"
# $Id: $
