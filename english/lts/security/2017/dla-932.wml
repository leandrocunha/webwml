<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>A vulnerability was discovered in Ghostscript, the GPL PostScript/PDF
interpreter, which may lead to the execution of arbitrary code or denial
of service if a specially crafted Postscript file is processed.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
9.05~dfsg-6.3+deb7u6.</p>

<p>We recommend that you upgrade your ghostscript packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-932.data"
# $Id: $
