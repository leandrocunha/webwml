<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-10324">CVE-2016-10324</a>

      <p>In libosip2 in GNU oSIP 4.1.0, a malformed SIP message can lead to
      a heap buffer overflow in the osip_clrncpy() function defined in
      osipparser2/osip_port.c.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-10325">CVE-2016-10325</a>

      <p>In libosip2 in GNU oSIP 4.1.0, a malformed SIP message can lead to a
      heap buffer overflow in the _osip_message_to_str() function defined
      in osipparser2/osip_message_to_str.c, resulting in a remote DoS.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-10326">CVE-2016-10326</a>

      <p>In libosip2 in GNU oSIP 4.1.0, a malformed SIP message can lead to
      a heap buffer overflow in the osip_body_to_str() function defined
      in osipparser2/osip_body.c, resulting in a remote DoS.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-7853">CVE-2017-7853</a>

      <p>In libosip2 in GNU oSIP 5.0.0, a malformed SIP message can lead to a
      heap buffer overflow in the msg_osip_body_parse() function defined
      in osipparser2/osip_message_parse.c, resulting in a remote DoS.</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
3.6.0-4+deb7u1.</p>

<p>We recommend that you upgrade your libosip2 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-898.data"
# $Id: $
