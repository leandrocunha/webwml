<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Michael Stepankin and Olga Barinova discovered a remote code execution
vulnerability in Apache Solr by exploiting XML External Entity
processing (XXE) in conjunction with use of a Config API add-listener
command to reach the RunExecutableListener class. To resolve this
issue the RunExecutableListener class has been removed and resolving
of external entities in the CoreParser class disallowed.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
3.6.0+dfsg-1+deb7u3.</p>

<p>We recommend that you upgrade your lucene-solr packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1254.data"
# $Id: $
