<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>dnsmasq, a DNS forwarder and DHCP server, ships the DNS Root Zone Key
Signing Key (KSK), used as the DNSSEC trust anchor. ICANN will rollover
the KSK in 11 October 2018, and DNS resolvers will need the new key
(KSK-2017) to continue performing DNSSEC validation. This dnsmasq package
update includes the latest key to prevent issues in scenarios where
dnsmasq runs with DNSSEC enabled and it is using the trusted anchors file
shipped with the package. Please note this is not the default
configuration in Debian.</p>

<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
2.72-3+deb8u4.</p>

<p>We recommend that you upgrade your dnsmasq packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1532.data"
# $Id: $
