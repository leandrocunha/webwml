<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>A stack-based buffer overflow vulnerability in sudo, a program designed
to provide limited super user privileges to specific users, triggerable
when configured with the pwfeedback option enabled. An unprivileged user
can take advantage of this flaw to obtain full root privileges.</p>


<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
1.8.10p3-1+deb8u7.</p>

<p>We recommend that you upgrade your sudo packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2094.data"
# $Id: $
