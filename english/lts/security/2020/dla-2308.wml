<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>In libopenmpt before 0.3.19 and 0.4.x before 0.4.9,
ModPlug_InstrumentName and ModPlug_SampleName in libopenmpt_modplug.c
do not restrict the lengths of libmodplug output-buffer strings in
the C API, leading to a buffer overflow.</p>

<p>For Debian 9 stretch, this problem has been fixed in version
0.2.7386~beta20.3-3+deb9u4.</p>

<p>We recommend that you upgrade your libopenmpt packages.</p>

<p>For the detailed security status of libopenmpt please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/libopenmpt">https://security-tracker.debian.org/tracker/libopenmpt</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2308.data"
# $Id: $
