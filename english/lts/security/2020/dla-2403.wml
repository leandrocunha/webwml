<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>A potential Cross-Site Scripting (XSS) vulnerability was found in rails,
a ruby based MVC framework. Views that allow the user to control the
default (not found) value of the `t` and `translate` helpers could be
susceptible to XSS attacks. When an HTML-unsafe string is passed as the
default for a missing translation key named html or ending in _html, the
default string is incorrectly marked as HTML-safe and not escaped.</p>

<p>For Debian 9 stretch, this problem has been fixed in version
2:4.2.7.1-1+deb9u4.</p>

<p>We recommend that you upgrade your rails packages.</p>

<p>For the detailed security status of rails please refer to
its security tracker page at:
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/rails">https://security-tracker.debian.org/tracker/rails</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2403.data"
# $Id: $
