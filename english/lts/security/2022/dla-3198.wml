<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>It was discovered that php-phpseclib, a pure-PHP implementation of
various cryptographic and arithmetic algorithms (v2), mishandles RSA
PKCS#1 v1.5 signature verification. An attacker may get invalid
signatures accepted, bypassing authorization control in specific
situations.</p>

<p>For Debian 10 buster, this problem has been fixed in version
2.0.30-2~deb10u1.</p>

<p>We recommend that you upgrade your php-phpseclib packages.</p>

<p>For the detailed security status of php-phpseclib please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/php-phpseclib">https://security-tracker.debian.org/tracker/php-phpseclib</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3198.data"
# $Id: $
