<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Two issues were found in GDAL, a geospatial library, that could lead
to denial of service via application crash or possibly the execution
of arbitrary code if maliciously crafted data was parsed.</p>

<p>For Debian 10 buster, these problems have been fixed in version
2.4.0+dfsg-1+deb10u1.</p>

<p>We recommend that you upgrade your gdal packages.</p>

<p>For the detailed security status of gdal please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/gdal">https://security-tracker.debian.org/tracker/gdal</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3129.data"
# $Id: $
