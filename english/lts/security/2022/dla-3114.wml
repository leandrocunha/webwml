<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several issues have been discovered in the MariaDB database server. The
vulnerabilities are addressed by upgrading MariaDB to the new upstream
version 10.3.36. Please see the MariaDB 10.3 Release Notes for further
details:</p>

<p>https://mariadb.com/kb/en/mariadb-10335-release-notes/
<a href="https://mariadb.com/kb/en/mariadb-10336-release-notes/">https://mariadb.com/kb/en/mariadb-10336-release-notes/</a></p>

<p>For Debian 10 buster, these problems have been fixed in version
1:10.3.36-0+deb10u1.</p>

<p>We recommend that you upgrade your mariadb-10.3 packages.</p>

<p>For the detailed security status of mariadb-10.3 please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/mariadb-10.3">https://security-tracker.debian.org/tracker/mariadb-10.3</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3114.data"
# $Id: $
