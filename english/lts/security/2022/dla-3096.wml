<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>A heap-based buffer over write vulnerability was found in GhostScript, the GPL
PostScript/PDF interpreter. An attacker could trick a user to open a crafted
PDF file, triggering the heap buffer overflow that could lead to memory
corruption or a denial of service.</p>

<p>For Debian 10 buster, this problem has been fixed in version
9.27~dfsg-2+deb10u6.</p>

<p>We recommend that you upgrade your ghostscript packages.</p>

<p>For the detailed security status of ghostscript please refer to
its security tracker page at:
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/ghostscript">https://security-tracker.debian.org/tracker/ghostscript</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3096.data"
# $Id: $
