<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities have been discovered in the Linux kernel that
may lead to a privilege escalation, denial of service or information
leaks.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-2585">CVE-2022-2585</a>

    <p>A use-after-free flaw in the implementation of POSIX CPU timers may
    result in denial of service or in local privilege escalation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-2586">CVE-2022-2586</a>

    <p>A use-after-free in the Netfilter subsystem may result in local
    privilege escalation for a user with the CAP_NET_ADMIN capability in
    any user or network namespace.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-2588">CVE-2022-2588</a>

    <p>Zhenpeng Lin discovered a use-after-free flaw in the cls_route
    filter implementation which may result in local privilege escalation
    for a user with the CAP_NET_ADMIN capability in any user or network
    namespace.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-26373">CVE-2022-26373</a>

    <p>It was discovered that on certain processors with Intel's Enhanced
    Indirect Branch Restricted Speculation (eIBRS) capabilities there
    are exceptions to the documented properties in some situations,
    which may result in information disclosure.</p>

    <p>Intel's explanation of the issue can be found at
    <url "https://www.intel.com/content/www/us/en/developer/articles/technical/software-security-guidance/advisory-guidance/post-barrier-return-stack-buffer-predictions.html"></p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-29900">CVE-2022-29900</a>

    <p>Johannes Wikner and Kaveh Razavi reported that for AMD/Hygon
    processors, mis-trained branch predictions for return instructions
    may allow arbitrary speculative code execution under certain
    microarchitecture-dependent conditions.</p>

    <p>A list of affected AMD CPU types can be found at
    <url "https://www.amd.com/en/corporate/product-security/bulletin/amd-sb-1037"></p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-29901">CVE-2022-29901</a>

    <p>Johannes Wikner and Kaveh Razavi reported that for Intel processors
    (Intel Core generation 6, 7 and 8), protections against speculative
    branch target injection attacks were insufficient in some
    circumstances, which may allow arbitrary speculative code execution
    under certain microarchitecture-dependent conditions.</p>

    <p>More information can be found at
    <url "https://www.intel.com/content/www/us/en/developer/articles/technical/software-security-guidance/advisory-guidance/return-stack-buffer-underflow.html"></p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-36879">CVE-2022-36879</a>

    <p>A flaw was discovered in xfrm_expand_policies in the xfrm subsystem
    which can cause a reference count to be dropped twice.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-36946">CVE-2022-36946</a>

    <p>Domingo Dirutigliano and Nicola Guerrera reported a memory
    corruption flaw in the Netfilter subsystem which may result in
    denial of service.</p></li>

</ul>

<p>For the stable distribution (bullseye), these problems have been fixed in
version 5.10.136-1.</p>

<p>We recommend that you upgrade your linux packages.</p>

<p>For the detailed security status of linux please refer to its security
tracker page at:
<a href="https://security-tracker.debian.org/tracker/linux">\
https://security-tracker.debian.org/tracker/linux</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2022/dsa-5207.data"
# $Id: $
