<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Three security vulnerabilities have been discovered in the Docker
container runtime: Insecure loading of NSS libraries in <q>docker cp</q>
could result in execution of code with root privileges, sensitive data
could be logged in debug mode and there was a command injection
vulnerability in the <q>docker build</q> command.</p>

<p>For the stable distribution (buster), these problems have been fixed in
version 18.09.1+dfsg1-7.1+deb10u1.</p>

<p>We recommend that you upgrade your docker.io packages.</p>

<p>For the detailed security status of docker.io please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/docker.io">\
https://security-tracker.debian.org/tracker/docker.io</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2019/dsa-4521.data"
# $Id: $
