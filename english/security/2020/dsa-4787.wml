<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Two vulnerabilities were discovered in moin, a Python clone of WikiWiki.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-15275">CVE-2020-15275</a>

    <p>Catarina Leite discovered that moin is prone to a stored XSS
    vulnerability via SVG attachments.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-25074">CVE-2020-25074</a>

    <p>Michael Chapman discovered that moin is prone to a remote code
    execution vulnerability via the cache action.</p></li>

</ul>

<p>For the stable distribution (buster), these problems have been fixed in
version 1.9.9-1+deb10u1.</p>

<p>We recommend that you upgrade your moin packages.</p>

<p>For the detailed security status of moin please refer to its security
tracker page at:
<a href="https://security-tracker.debian.org/tracker/moin">https://security-tracker.debian.org/tracker/moin</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2020/dsa-4787.data"
# $Id: $
