#use wml::debian::template title="Inviare informazioni sui mirror"
#use wml::debian::translation-check translation="cd8f39027a950a5174d2d49109ba6800c0f0cdec" maintainer="skizzhg"
#include "$(ENGLISHDIR)/mirror/submit.inc"

<p>Chi desidera inviare informazioni su un mirror di Debian, può
farlo utilizzando il modulo seguente. Le
informazioni fornite verranno visualizzate nell'elenco dei mirror.
</p>

<p>Notare che tutti i mirror presentati devono seguire le <a
href="ftpmirror">regole e le indicazioni della nostra documentazione</a>.
In particolare,
</p>
<ul>
<li>deve usare <a href="ftpmirror#how">ftpsync</a> per l'aggiornamento,</li>
<li>scegliere un buon sorgente (<strong>non</strong> un nome di servizio
  (http) come <code>ftp.CC.debian.org</code>, non un DNS con round robin,
  non dei CDN),</li>
<li>aggiornare quattro volte al giorno per allinearsi alla frequenza
  d'aggiornamento dell'archivio (oppure impostare dei trigger con il
  sorgente, usare <code>ftpsync-cron</code> ogni ora per monitorare le
  modifiche al sorgente e avviare le sincronizzazioni). Inoltre,</li>
<li>il proprio mirror deve avere un file con nome tracefile (se ne occuper&agrave;
  ftpsync se MIRRORNAME è impostato correttamente),</li>
<li>creare i file <code>/Archive-Update-in-Progress-XXX</code> e
  <code>/Archive-Update-Required-XXX</code> quando opportuno (di nuovo,
  ftpsync lo fa al vostro posto) per agevolare i mirror che si aggiornano
  dal vostro a sincronizzarsi correttamente.</li>
</ul>

<form-action "" archive-upstream https://cgi.debian.org/cgi-bin/submit_mirror.pl>

<h2>Informazioni di base</h2>

<p>
<input type="radio" name="submissiontype" value="new" checked>
Inserimento nuovo mirror
&nbsp; &nbsp; &nbsp;
<input type="radio" name="submissiontype" value="update">
Aggiornamento di un mirror esistente
</p>

<p>
Nome mirror: <input type="text" name="site" size="30"></p>

<p>Immettere nei campi sottostanti i percorsi per il mirror Debian del
vostro sito. Lasciare vuoti i campi non pertinenti.</p>

<table>
<tr>
  <td>Archivio dei pacchetti, via HTTP: </td>
  <td><input type="text" name="archive-http" id="archve-http" size="30"
    value="/debian" readonly="readonly">
  <small>L'archivio deve essere disponibile a
  <code>/debian</code>.</small></td>
</tr>
<tablerowdef "Archivio dei pacchetti, via rsync"  archive-rsync  30 "debian" " <small>Se si offre rsync, si suggerisce di usare <code>debian</code> come nome del modulo.</small>">
# <tablerow "Immagini dei CD/DVD, via HTTP"      cdimage-http   30>
# <tablerow "Immagini dei CD/DVD, via rsync"     cdimage-rsync  30>
# <tablerow "Vecchi rilasci Debian, via HTTP"  old-http     30>
# <tablerow "Vecchi rilasci Debian, via rsync" old-rsync    30>
</table>

<h2>Informazioni sul sito mirror</h2>

<table>
<tr>
<td>Architetture disponibili:
<td>
<label><input type=checkbox name=architectures id="allarch" value="ALL" onclick="allarches()">&nbsp;<em>tutte (ovvero nessuna esclusa)</em></label><br>
<archlist>
</td></tr>
</table>

<table>
<tablerow "Nome del responsabile del sito"         maint_name    30>
<tablerow "Indirizzo email pubblico del responsabile del sito"              maint_public_email    30>
<tr><td>Paese del sito:  <td><select name="country">
<countrylist>
</select>
<tablerow "Ubicazione del sito (facoltativo)"     location      30>
<tablerow "Nome dello sponsor del sito (facoltativo)" sponsor_name  30>
<tablerow "URL del sito dello sponsor (facoltativo)"  sponsor_url   30>
</table>

<table><tr>
<td valign="top">Commento:</td>
<td><textarea name="comment" cols=40 rows=7></textarea></td>
</tr></table>

<p><label>Ho effettuato l'iscrizione alla mailing list <a
href="https://lists.debian.org/debian-mirrors-announce/"></a>
<input type="checkbox" name="mlannounce"></label>
</p>

<p>
<input type="submit" value="Submit"> <input type="reset" value="Clear form">
</p>
</form>

<p>Il sito dovrebbe apparire nell'elenco entro un paio di settimane,
appena sarà verificato e incluso da un operatore. In caso di problemi con
quanto inviato potremmo contattarvi tramite email.</p>

<p>Se non ottiene risposta entro tre mesi potete contattarci
all'indirizzo <email mirrors@debian.org>.</p>
