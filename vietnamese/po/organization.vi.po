#
# Trần Ngọc Quân <vnwildman@gmail.com>, 2015.
#
msgid ""
msgstr ""
"Project-Id-Version: webwml organization\n"
"PO-Revision-Date: 2015-06-22 15:18+0700\n"
"Last-Translator: Trần Ngọc Quân <vnwildman@gmail.com>\n"
"Language-Team: Vietnamese <debian-l10n-vietnamese@lists.debian.org>\n"
"Language: vi\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Gtranslator 2.91.7\n"

#: ../../english/intro/organization.data:15
msgid "delegation mail"
msgstr "thư đại biểu"

#: ../../english/intro/organization.data:16
msgid "appointment mail"
msgstr "thư bổ nhiệm"

#. One male delegate
#. Pronoun tags with delegate combinations
#: ../../english/intro/organization.data:18
#: ../../english/intro/organization.data:22
msgid "<void id=\"male\"/>delegate"
msgstr "<void id=\"male\"/>đại biểu"

#. One female delegate
#: ../../english/intro/organization.data:20
#: ../../english/intro/organization.data:23
msgid "<void id=\"female\"/>delegate"
msgstr "<void id=\"female\"/>đại biểu"

#: ../../english/intro/organization.data:22
#: ../../english/intro/organization.data:25
#, fuzzy
#| msgid "<void id=\"female\"/>delegate"
msgid "<void id=\"he_him\"/>he/him"
msgstr "<void id=\"female\"/>đại biểu"

#: ../../english/intro/organization.data:23
#: ../../english/intro/organization.data:26
#, fuzzy
#| msgid "<void id=\"female\"/>delegate"
msgid "<void id=\"she_her\"/>she/her"
msgstr "<void id=\"female\"/>đại biểu"

#: ../../english/intro/organization.data:24
#, fuzzy
#| msgid "<void id=\"female\"/>delegate"
msgid "<void id=\"gender_neutral\"/>delegate"
msgstr "<void id=\"female\"/>đại biểu"

#: ../../english/intro/organization.data:24
#: ../../english/intro/organization.data:27
#, fuzzy
#| msgid "<void id=\"female\"/>delegate"
msgid "<void id=\"they_them\"/>they/them"
msgstr "<void id=\"female\"/>đại biểu"

#: ../../english/intro/organization.data:30
#: ../../english/intro/organization.data:32
msgid "current"
msgstr "hiện tại"

#: ../../english/intro/organization.data:34
#: ../../english/intro/organization.data:36
msgid "member"
msgstr "thành viên"

#: ../../english/intro/organization.data:39
msgid "manager"
msgstr "nhà quản lý"

#: ../../english/intro/organization.data:41
msgid "SRM"
msgstr "SRM"

#: ../../english/intro/organization.data:41
msgid "Stable Release Manager"
msgstr "Quản lý phát hành bản ổn định"

#: ../../english/intro/organization.data:43
msgid "wizard"
msgstr "thuật sỹ"

#. we only use the chair tag once, for techctte, I wonder why it's here.
#: ../../english/intro/organization.data:45
#, fuzzy
msgid "chair"
msgstr "chủ tịch"

#: ../../english/intro/organization.data:48
msgid "assistant"
msgstr "trợ lý"

#: ../../english/intro/organization.data:50
msgid "secretary"
msgstr "thư ký"

#: ../../english/intro/organization.data:52
msgid "representative"
msgstr ""

#: ../../english/intro/organization.data:54
msgid "role"
msgstr ""

#: ../../english/intro/organization.data:62
msgid ""
"In the following list, <q>current</q> is used for positions that are\n"
"transitional (elected or appointed with a certain expiration date)."
msgstr ""

#: ../../english/intro/organization.data:70
#: ../../english/intro/organization.data:82
msgid "Officers"
msgstr "Các nhân viên"

#: ../../english/intro/organization.data:71
#: ../../english/intro/organization.data:108
msgid "Distribution"
msgstr "Phân phối"

#: ../../english/intro/organization.data:72
#: ../../english/intro/organization.data:204
msgid "Communication and Outreach"
msgstr ""

#: ../../english/intro/organization.data:74
#: ../../english/intro/organization.data:207
msgid "Data Protection team"
msgstr ""

#: ../../english/intro/organization.data:75
#: ../../english/intro/organization.data:212
#, fuzzy
msgid "Publicity team"
msgstr "Publicity"

#: ../../english/intro/organization.data:77
#: ../../english/intro/organization.data:279
msgid "Membership in other organizations"
msgstr ""

#: ../../english/intro/organization.data:78
#: ../../english/intro/organization.data:302
msgid "Support and Infrastructure"
msgstr "Hỗ trợ và Cơ sở hạ tầng"

#: ../../english/intro/organization.data:85
msgid "Leader"
msgstr "Lãnh đạo"

#: ../../english/intro/organization.data:87
msgid "Technical Committee"
msgstr "Ủy ban kỹ thuật"

#: ../../english/intro/organization.data:103
msgid "Secretary"
msgstr "Thư ký"

#: ../../english/intro/organization.data:111
msgid "Development Projects"
msgstr "Các dự án phát triển"

#: ../../english/intro/organization.data:112
msgid "FTP Archives"
msgstr "Lưu trữ FTP"

#: ../../english/intro/organization.data:114
msgid "FTP Masters"
msgstr "Chủ FPT"

#: ../../english/intro/organization.data:120
msgid "FTP Assistants"
msgstr "Trợ lý FTP"

#: ../../english/intro/organization.data:126
msgid "FTP Wizards"
msgstr "Thuật sỹ FTP"

#: ../../english/intro/organization.data:130
msgid "Backports"
msgstr "Chuyển ngược"

#: ../../english/intro/organization.data:132
msgid "Backports Team"
msgstr "Nhóm chuyển ngược"

#: ../../english/intro/organization.data:136
msgid "Release Management"
msgstr "Ban quản lý phát hành"

#: ../../english/intro/organization.data:138
msgid "Release Team"
msgstr "Nhóm phát hành"

#: ../../english/intro/organization.data:148
msgid "Quality Assurance"
msgstr "Đảm bảo chất lượng"

#: ../../english/intro/organization.data:149
msgid "Installation System Team"
msgstr "Nhóm Hệ thống cài đặt"

#: ../../english/intro/organization.data:150
msgid "Debian Live Team"
msgstr ""

#: ../../english/intro/organization.data:151
msgid "Release Notes"
msgstr "Ghi chú phát hành"

#: ../../english/intro/organization.data:153
#, fuzzy
#| msgid "CD Images"
msgid "CD/DVD/USB Images"
msgstr "Ảnh CD"

#: ../../english/intro/organization.data:155
msgid "Production"
msgstr "Sản xuất"

#: ../../english/intro/organization.data:162
msgid "Testing"
msgstr "Thử nghiệm"

#: ../../english/intro/organization.data:164
msgid "Cloud Team"
msgstr ""

#: ../../english/intro/organization.data:168
msgid "Autobuilding infrastructure"
msgstr "Cơ sở hạ tầng tự động biên dịch"

#: ../../english/intro/organization.data:170
msgid "Wanna-build team"
msgstr "Nhóm Wanna-build"

#: ../../english/intro/organization.data:177
msgid "Buildd administration"
msgstr "Quản tri Buildd"

#: ../../english/intro/organization.data:194
msgid "Documentation"
msgstr "Tài liệu"

#: ../../english/intro/organization.data:199
msgid "Work-Needing and Prospective Packages list"
msgstr "Danh sách các gói cần làm và trong tương lai"

#: ../../english/intro/organization.data:215
msgid "Press Contact"
msgstr "Liên hệ báo chí"

#: ../../english/intro/organization.data:217
msgid "Web Pages"
msgstr "Trang thông tin điện tử"

#: ../../english/intro/organization.data:225
msgid "Planet Debian"
msgstr "Planet Debian"

#: ../../english/intro/organization.data:230
msgid "Outreach"
msgstr "Hoạt động ngoài lề"

#: ../../english/intro/organization.data:235
msgid "Debian Women Project"
msgstr "Dự án Phụ nữ Debian"

#: ../../english/intro/organization.data:243
msgid "Community"
msgstr ""

#: ../../english/intro/organization.data:250
msgid ""
"To send a private message to all the members of the Community Team, use the "
"GPG key <a href=\"community-team-pubkey.txt"
"\">817DAE61E2FE4CA28E1B7762A89C4D0527C4C869</a>."
msgstr ""

#: ../../english/intro/organization.data:252
msgid "Events"
msgstr "Sự kiện"

#: ../../english/intro/organization.data:259
#, fuzzy
msgid "DebConf Committee"
msgstr "Ủy ban kỹ thuật"

#: ../../english/intro/organization.data:266
msgid "Partner Program"
msgstr "Chương trình đối tác"

#: ../../english/intro/organization.data:270
msgid "Hardware Donations Coordination"
msgstr "Điều phối các quyên góp phần cứng"

#: ../../english/intro/organization.data:285
msgid "GNOME Foundation"
msgstr ""

#: ../../english/intro/organization.data:287
msgid "Linux Professional Institute"
msgstr ""

#: ../../english/intro/organization.data:288
msgid "Linux Magazine"
msgstr ""

#: ../../english/intro/organization.data:290
msgid "Linux Standards Base"
msgstr ""

#: ../../english/intro/organization.data:291
msgid "Free Standards Group"
msgstr ""

#: ../../english/intro/organization.data:292
msgid ""
"OASIS: Organization\n"
"      for the Advancement of Structured Information Standards"
msgstr ""

#: ../../english/intro/organization.data:295
msgid ""
"OVAL: Open Vulnerability\n"
"      Assessment Language"
msgstr ""

#: ../../english/intro/organization.data:298
msgid "Open Source Initiative"
msgstr ""

#: ../../english/intro/organization.data:305
msgid "Bug Tracking System"
msgstr "Hệ thống theo dõi lỗi"

#: ../../english/intro/organization.data:310
msgid "Mailing Lists Administration and Mailing List Archives"
msgstr "Quản trị và Lưu trữ các bó thư"

#: ../../english/intro/organization.data:318
msgid "New Members Front Desk"
msgstr "Các thành viên Bàn lễ tân mới"

#: ../../english/intro/organization.data:324
msgid "Debian Account Managers"
msgstr "Quản lý các tài khoản Debian"

#: ../../english/intro/organization.data:328
msgid ""
"To send a private message to all DAMs, use the GPG key "
"57731224A9762EA155AB2A530CA8D15BB24D96F2."
msgstr ""

#: ../../english/intro/organization.data:329
msgid "Keyring Maintainers (PGP and GPG)"
msgstr "Những nhà bảo trì chùm chìa khóa (PGP và GPG)"

#: ../../english/intro/organization.data:333
msgid "Security Team"
msgstr "Nhóm bảo mật"

#: ../../english/intro/organization.data:346
msgid "Policy"
msgstr "Chính sách"

#: ../../english/intro/organization.data:349
msgid "System Administration"
msgstr "Quản trị hệ thống"

#: ../../english/intro/organization.data:350
msgid ""
"This is the address to use when encountering problems on one of Debian's "
"machines, including password problems or you need a package installed."
msgstr ""
"Đây là địa chỉ dùng khi gặp trục trặc với một trong số các máy của Debian, "
"bao gồm các vấn đề với mật khẩu hoặc bạn muốn một gói nào đó được cài đặt."

#: ../../english/intro/organization.data:359
msgid ""
"If you have hardware problems with Debian machines, please see <a href="
"\"https://db.debian.org/machines.cgi\">Debian Machines</a> page, it should "
"contain per-machine administrator information."
msgstr ""
"Nếu bạn có trục trặc phần cứng với các máy Debian, vui lòng xem trang <a "
"href=\"https://db.debian.org/machines.cgi\">Các máy Debian</a>, nó chứa "
"thông tin quản trị cho từng máy."

#: ../../english/intro/organization.data:360
msgid "LDAP Developer Directory Administrator"
msgstr "Nhà quản trị Thư mục Phát triển LDAP"

#: ../../english/intro/organization.data:361
msgid "Mirrors"
msgstr "Máy bản sao"

#: ../../english/intro/organization.data:364
msgid "DNS Maintainer"
msgstr "Quản trị DNS"

#: ../../english/intro/organization.data:365
msgid "Package Tracking System"
msgstr "Hệ thống theo dõi lỗi"

#: ../../english/intro/organization.data:367
msgid "Treasurer"
msgstr ""

#: ../../english/intro/organization.data:374
msgid ""
"<a name=\"trademark\" href=\"m4_HOME/trademark\">Trademark</a> use requests"
msgstr ""
"Các yêu cầu dùng <a name=\"trademark\" href=\"m4_HOME/trademark\">nhãn hiệu</"
"a>"

#: ../../english/intro/organization.data:378
#, fuzzy
msgid "Salsa administrators"
msgstr "Các quản trị viên Alioth"

#~ msgid "Alioth administrators"
#~ msgstr "Các quản trị viên Alioth"

#~ msgid "Anti-harassment"
#~ msgstr "Chống quấy rối"

#~ msgid "Auditor"
#~ msgstr "Kiểm soát"

#~ msgid "Bits from Debian"
#~ msgstr "Bits từ Debian"

#~ msgid "CD Vendors Page"
#~ msgstr "Trang nhà cung cấp CD"

#~ msgid "Consultants Page"
#~ msgstr "Trang cố vấn"

#~ msgid "Custom Debian Distributions"
#~ msgstr "Bạn phân phối Debian tùy chỉnh"

#~ msgid "DebConf chairs"
#~ msgstr "Các ghế DebConf"

#~ msgid "Debian Maintainer (DM) Keyring Maintainers"
#~ msgstr "Những người quản lý chùm chìa khóa Nhà bảo trì Debian (DM)"

#~ msgid "Debian Pure Blends"
#~ msgstr "Debian Pure Blends"

#, fuzzy
#~| msgid "Debian for education"
#~ msgid "Debian for astronomy"
#~ msgstr "Debian dành cho giáo dục"

#~ msgid "Debian for children from 1 to 99"
#~ msgstr "Debian dành cho trẻ em từ 1 đến 99"

#~ msgid "Debian for education"
#~ msgstr "Debian dành cho giáo dục"

#~ msgid "Debian for medical practice and research"
#~ msgstr "Debian dành cho nghiên cứu và thực hành y học"

#~ msgid "Debian for people with disabilities"
#~ msgstr "Debian dành cho những người khuyết tật"

#~ msgid "Debian for science and related research"
#~ msgstr "Debian dành cho khoa học và nghiên cứu"

#~ msgid "Debian in legal offices"
#~ msgstr "Debian trong các văn phòng luật"

#~ msgid "Embedded systems"
#~ msgstr "Các hệ thống nhúng"

#~ msgid "Firewalls"
#~ msgstr "Tường lửa"

#~ msgid "Individual Packages"
#~ msgstr "Các gói riêng lẻ"

#~ msgid "Laptops"
#~ msgstr "Máy tính xách tay"

#~ msgid "Live System Team"
#~ msgstr "Nhóm hệ thống Live"

#~ msgid "Marketing Team"
#~ msgstr "Nhóm tiếp cận thị trường"

#~ msgid "Ports"
#~ msgstr "Bản chuyển đổi"

#~ msgid "Publicity"
#~ msgstr "Publicity"

#~ msgid "Release Team for ``stable''"
#~ msgstr "Nhóm phát hành cho bản ổn định ``stable''"

#~ msgid "Special Configurations"
#~ msgstr "Các cấu hình đặc biệt"

#~ msgid "User support"
#~ msgstr "Hỗ trợ người dùng"

#~ msgid "current Debian Project Leader"
#~ msgstr "Nhà lãnh đạo dự án Debian hiện tại"
