#use wml::debian::translation-check translation="f3b1664a6ad1c717b34a049e88e8b8c8dccace4c" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Un problème a été découvert dans openvswitch, un commutateur virtuel
Ethernet à base logicielle.</p>

<p>Ce problème concerne un dépassement de tampon de tas dans flow.c, qui
pouvait conduire à un accès à une zone mémoire non mappée. Cela pouvait
avoir pour conséquence le plantage du logiciel, une modification de la
mémoire ou une potentielle exécution à distance.</p>

<p>Pour Debian 10 Buster, ce problème a été corrigé dans la version
2.10.7+ds1-0+deb10u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets openvswitch.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de openvswitch,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/openvswitch">\
https://security-tracker.debian.org/tracker/openvswitch</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3168.data"
# $Id: $
