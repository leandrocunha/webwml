#use wml::debian::translation-check translation="707fdf79d00a08fc8b8256b06abc197b17506c50" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Un défaut a été découvert dans la fonction check_chunk_name() de
pngcheck, un outil pour vérifier l'intégrité des fichiers PNG, JNG et MNG.
Ce défaut permet à un attaquant qui peut transmettre un fichier malveillant
pour traitement à pngcheck de provoquer un déni de service temporaire.</p>

<p>Pour Debian 9 <q>Stretch</q>, ce problème a été corrigé dans la version
2.3.0-7+deb9u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets pngcheck.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de pngcheck, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/pngcheck">\
https://security-tracker.debian.org/tracker/pngcheck</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3032.data"
# $Id: $
