#use wml::debian::translation-check translation="4f17cbe55872e6ff372130e7a721c5ad6ae3beba" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités de sécurité ont été découvertes dans Asterisk, un
autocommutateur téléphonique privé (PBX) au code source ouvert. Des dépassements
de tampon et d'autres erreurs de programmation pouvaient être exploités pour la
divulgation d'informations ou l'exécution de code arbitraire.</p>

<p>Un soin particulier doit être pris lors de la mise à niveau vers cette
nouvelle version amont. Certains fichiers et options de configuration ont été
modifiés afin de remédier à certaines vulnérabilités de sécurité.
Principalement, le récepteur TLS pjsip n'accepte que des connexions TLSv1.3 dans
la configuration par défaut actuelle. Cela peut être annulé en ajoutant
« method=tlsv1_2 » au transport dans pjsip.conf. <a rel="nofollow" href="https://issues.asterisk.org/jira/browse/ASTERISK-29017">https://issues.asterisk.org/jira/browse/ASTERISK-29017</a>.</p>

<p>Pour Debian 10 « Buster », ces problèmes ont été corrigés dans
la version 1:16.28.0~dfsg-0+deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets asterisk.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de asterisk,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/asterisk">\
https://security-tracker.debian.org/tracker/asterisk</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur : <a rel="nofollow
"href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3194.data"
# $Id: $
