#use wml::debian::translation-check translation="4bfbe5d5f67795a8fece9987c96d6d1ae2ad0c96" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Il y avait une vulnérabilité potentielle d'attaque par injection de code
SQL dans Django, un cadriciel populaire de développement web basé sur
Python :</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-28346">CVE-2022-28346</a>

<p>CVE-2022-28346 : Un problème a été découvert dans les versions de
Django 2.2 antérieures à 2.2.28, 3.2 antérieures à 3.2.13 et
4.0 antérieures à 4.0.4. Les méthodes QuerySet.annotate(), aggregate() et
extra() sont sujettes à une injection de code SQL dans les alias de
colonnes à l'aide d'un dictionnaire contrefait (avec une expansion
dictionnaire) en tant qu'argument **kwargs passés à ces méthodes.</p></li>

</ul>

<p>Pour Debian 9 <q>Stretch</q>, ce problème a été corrigé dans la version
1:1.10.7-2+deb9u16.</p>

<p>Nous vous recommandons de mettre à jour vos paquets python-django.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-2982.data"
# $Id: $
