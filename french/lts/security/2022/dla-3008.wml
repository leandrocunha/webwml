#use wml::debian::translation-check translation="db2c7e758e7c6b30707e072868547b05e722380f" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Le script c_rehash ne nettoie pas correctement les métacaractères de
l’interpréteur de commande pour empêcher les injections de commande. Ce
script est exécuté par update-ca-certificates, à partir des certificats
d'autorités de certification, pour renouveler le hachage des certificats
dans /etc/ssl/certs/. Un attaquant capable de placer des fichiers dans ce
répertoire pourrait exécuter des commandes arbitraires avec les privilèges
du script.</p>

<p>Pour Debian 9 <q>Stretch</q>, ce problème a été corrigé dans la version
1.1.0l-1~deb9u6.</p>

<p>Nous vous recommandons de mettre à jour vos paquets openssl.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de openssl, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/openssl">\
https://security-tracker.debian.org/tracker/openssl</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3008.data"
# $Id: $
