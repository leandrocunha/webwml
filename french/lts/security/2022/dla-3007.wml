#use wml::debian::translation-check translation="427b37e5c38ae1197e59ac6c18371be24d1133ce" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo></p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3596">CVE-2021-3596</a>

<p>Un défaut de déréférencement de pointeur NULL a été découvert dans les
versions d'ImageMagick antérieures à 7.0.10-31 dans ReadSVGImage() de
coders/svg.c. Ce problème est dû à l'absence de vérification de la valeur
renvoyée par xmlCreatePushParserCtxt() de libxml2 et à l'utilisation
directe de la valeur, ce qui mène à une erreur de segmentation et un
plantage.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-28463">CVE-2022-28463</a>

<p>ImageMagick est vulnérable à un dépassement de tampon.</p></li>

</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans la
version 8:6.9.7.4+dfsg-11+deb9u14.</p>

<p>Nous vous recommandons de mettre à jour vos paquets imagemagick.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de imagemagick,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/imagemagick">\
https://security-tracker.debian.org/tracker/imagemagick</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3007.data"
# $Id: $
