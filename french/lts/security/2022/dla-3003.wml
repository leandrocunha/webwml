#use wml::debian::translation-check translation="a037a8f61465f31bf03b65ba41c953957acedaba" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Il y avait une attaque potentielle par déni de service dans
ruby-nokogiri, un analyseur HTML, XML, SAX, etc. écrit en langage de
programmation Ruby et pour ce langage. Cela était causé par l'utilisation
d'expressions rationnelles inefficaces qui étaient exposées à un retour sur
trace excessif.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-24836">CVE-2022-24836</a>

<p>Nokogiri est une bibliothèque XML et HTML à source ouvert pour Ruby.
Nokogiri, dans ses versions inférieures à v1.13.4 contient une expression
rationnelle inefficace qui est exposée à un retour sur trace excessif
lorsqu'il tente de détecter l'encodage dans les documents HTML. Il est
conseillé à tous les utilisateurs de mettre à niveau Nokogiri vers une
version &gt;= 1.13.4. Il n'y a pas de palliatif connu pour ce problème.</p></li>

</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans la
version 1.6.8.1-1+deb9u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets ruby-nokogiri.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3003.data"
# $Id: $
