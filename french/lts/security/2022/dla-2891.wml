#use wml::debian::translation-check translation="3af6e1976ff509a48f1fa508d787fd0b245345a3" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans le langage de
programmation Go. Un attaquant pourrait déclencher un déni de service (DoS)
et une fuite d'informations.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-33196">CVE-2021-33196</a>

<p>Dans archive/zip, un compte de fichier contrefait (dans un en-tête
d'archive) peut provoquer une panique de NewReader ou d'OpenReader.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-36221">CVE-2021-36221</a>

<p>Go présente une situation de compétition qui peut conduire à une panique
de ReverseProxy de net/http/httputil lors d'une interruption de
ErrAbortHandler.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-39293">CVE-2021-39293</a>

<p>Suite de la correction de
<a href="https://security-tracker.debian.org/tracker/CVE-2021-33196">CVE-2021-33196</a></p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-41771">CVE-2021-41771</a>

<p>ImportedSymbols dans debug/macho (pour Open ou OpenFat) accède à un
emplacement de mémoire au-delà de la fin d'un tampon, autrement dit une
situation de « slice » hors limites.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-44716">CVE-2021-44716</a>

<p>net/http permet une consommation non contrôlée de mémoire dans le cache
de mise en forme canonique d'en-tête au moyen de requêtes HTTP/2.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-44717">CVE-2021-44717</a>

<p>Go sur UNIX permet d'écrire des opérations sur un fichier non prévu ou
sur une connexion réseau non prévue comme conséquence de la fermeture
erronée du descripteur de fichier 0 après l'épuisement des descripteurs de
fichier.</p></li>

</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans la
version 1.8.1-1+deb9u4.</p>

<p>Nous vous recommandons de mettre à jour vos paquets golang-1.8.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de golang-1.8, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/golang-1.8">\
https://security-tracker.debian.org/tracker/golang-1.8</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-2891.data"
# $Id: $
