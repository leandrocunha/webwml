#use wml::debian::translation-check translation="abc49babd085d42b92412d53db842f5fee5952e8" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été corrigées dans des utilitaires de fig2dev
pour convertir des fichiers de dessin avec XFig.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-21529">CVE-2020-21529</a>

<p>Dépassement de tampon de pile dans bezier_spline().</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-21531">CVE-2020-21531</a>

<p>Dépassement global de tampon dans conv_pattern_index().</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-21532">CVE-2020-21532</a>

<p>Dépassement global de tampon dans setfigfont().</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-21676">CVE-2020-21676</a>

<p>Dépassement de pile dans genpstrx_text().</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-32280">CVE-2021-32280</a>

<p>Déréférencement de pointeur NULL dans compute_closed_spline().</p></li>

</ul>

<p>Pour Debian 10 <q>Buster</q>, ces problèmes ont été corrigés dans
la version 1:3.2.7a-5+deb10u5.</p>

<p>Nous vous recommandons de mettre à jour vos paquets fig2dev.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de fig2dev,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/fig2dev">\
https://security-tracker.debian.org/tracker/fig2dev</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3304.data"
# $Id: $
