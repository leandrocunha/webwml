#use wml::debian::translation-check translation="6aad813414b79293df418bab41e923ec7b32299b" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été corrigées dans la bibliothèque libstb.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-16981">CVE-2018-16981</a>

<p>Dépassement de tampon de tas dans stbi__out_gif_code().</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-13217">CVE-2019-13217</a>

<p>Dépassement de tampon de tas dans start_decoder() de Vorbis.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-13218">CVE-2019-13218</a>

<p>Division par zéro dans predict_point() de Vorbis .</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-13219">CVE-2019-13219</a>

<p>Déréférencement de pointeur NULL dans get_window() de Vorbis .</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-13220">CVE-2019-13220</a>

<p>Variables de pile non initialisées dans start_decoder() de Vorbis.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-13221">CVE-2019-13221</a>

<p>Dépassement de tampon dans compute_codewords() de Vorbis.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-13222">CVE-2019-13222</a>

<p>Lecture hors limites dans le tampon global de draw_line() de Vorbis.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-13223">CVE-2019-13223</a>

<p>Assertion accessible dans lookup1_values() de Vorbis.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-28021">CVE-2021-28021</a>

<p>Dépassement de tampon dans stbi__extend_receive() de Vorbis.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-37789">CVE-2021-37789</a>

<p>Dépassement de tampon de tas dans stbi__jpeg_load().</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-42715">CVE-2021-42715</a>

<p>Le chargeur HDR analysait des lignes de balayage RLE sans fins de ligne
comme une séquence infinie de séquences de longueur zéro.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-28041">CVE-2022-28041</a>

<p>Dépassement d'entier dans stbi__jpeg_decode_block_prog_dc().</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-28042">CVE-2022-28042</a>

<p>Utilisation de mémoire après libération basée sur le tas dans
stbi__jpeg_huff_decode().</p></li>

</ul>

<p>Pour Debian 10 <q>Buster</q>, ces problèmes ont été corrigés dans
la version 0.0~git20180212.15.e6afb9c-1+deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets libstb.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de libstb,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/libstb">\
https://security-tracker.debian.org/tracker/libstb</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3305.data"
# $Id: $
