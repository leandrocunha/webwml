#use wml::debian::translation-check translation="3c6c36b12d8fce77e606578dfdf60ae7b711471a" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs problèmes ont été trouvés dans modsecurity-crs, un ensemble de
règles génériques de détection d’attaque utilisé avec ModSecurity ou des
pare-feux d’application web compatible. Cela permettait à des attaquants
distants de contourner le pare-feu d’applications web.</p>

<p>Lors de l’utilisation de modsecurity-crs avec apache2/libapache2-modsecurity,
assurez-vous de revoir votre configuration de modsecurity, habituellement
/etc/modsecurity/modsecurity.conf, en vous servant de la configuration
recommandée mise à jour, disponible dans
/etc/modsecurity/modsecurity.conf-recommended : certaines des modifications des
règles recommandées sont requises pour éviter le contournement de pare-feu
d’applications web dans certaines circonstances.</p>

<p>Veuillez remarquer que
<a href="https://security-tracker.debian.org/tracker/CVE-2022-39956">CVE-2022-39956</a> \
nécessite un paquet modsecurity-apache mis à jour, qui a été précédemment
téléversé dans Buster-security, consultez l’annonce LTS DLA-3283-1 de Debian
pour plus de détails.</p>

<p>Si vous utilisez une autre solution en connexion avec modsecurity-ruleset,
par exemple, une qui utilise libmodsecurity3, votre solution peut produire une
erreur avec un message tel que <q>Error creating rule: Unknown variable:
MULTIPART_PART_HEADERS</q>. Dans ce cas, vous pouvez désactiver la mitigation
pour le <a href="https://security-tracker.debian.org/tracker/CVE-2022-29956">CVE-2022-29956</a> \
en supprimant le fichier REQUEST-922-MULTIPART-ATTACK.conf. Cependant, soyez
conscients que cela désactivera la protection et pourrait permettre à des
attaquants de contourner votre pare-feu d’application web.</p>

<p>Il n’y a pas de paquet dans Debian qui dépend de libmodsecurity3, aussi si
vous n’avez que des logiciels de Debian, vous n’êtes pas touché par cette
limitation.</p>

<p>Merci à @airween pour l’aide pendant la préparation de cette mise à jour.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-16384">CVE-2018-16384</a>

<p>Un contournement d’injection SQL (c’est-à-dire, contournement PL1) existait
dans l’ensemble de règles centrales de ModSecurity d’OWASP
(owasp-modsecurity-crs) jusqu’à la version 3.1.0-rc3 à l’aide de {`a`b} où
a est un nom spécial de fonction (tel que <q>if</q>) et b est une déclaration
SQL à exécuter.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-22669">CVE-2020-22669</a>

<p>Modsecurity owasp-modsecurity-crs 3.2.0 (niveau PL1 de paranoïa) avait une
vulnérabilité de contournement d’injection SQL. Des attaquants pouvaient
utiliser les caractères de commentaire et des affectations de variable dans la
syntaxe SQL pour contourner la protection WAF de Modsecurity et mettre en œuvre
une attaque par injection SQL dans des applications web.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-39955">CVE-2022-39955</a>

<p>L’ensemble de règles centrales de ModSecurity d’OWASP (CRS) était sujet à un
contournement partiel lors de la soumission d’un champ d’en-tête Content-Type
HTTP contrefait pour l'occasion qui indiquait plusieurs schémas d’encodage de
caractères. Un dorsal vulnérable pouvait potentiellement être exploité en
déclarant plusieurs noms <q>charset</q> de Content-Type et par conséquent
contourner la liste permise de <q>charset</q> configurable de l’en-tête
Content-Type CRS. Une charge utile encodée pouvait contourner la détection CRS
de cette façon et pouvait être alors décodée par le dorsal. Les anciennes
versions 3.0.x et 3.1.x de CRS sont affectées, ainsi que les versions 3.2.1 et
3.3.2 actuellement gérées. Les intégrateurs et les utilisateurs seraient
bien avisés de mettre à niveau vers les versions 3.2.2 et 3.3.3 respectivement.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-39956">CVE-2022-39956</a>

<p>L’ensemble de règles centrales de ModSecurity d’OWASP (CRS) était sujet à un
contournement partiel de requêtes multiparties HTTP en soumettant une charge
utile qui utilise un schéma d’encodage de caractères à l’aide de Content-Type ou
des champs obsolètes d’en-tête MIME multipartie Content-Transfer-Encoding, qui
ne seront pas décodés et inspectés par le moteur de pare-feu de l’application
web et l’ensemble de règles. La charge multipartie contournait alors la
détection. Un dorsal vulnérable qui gérait ces schémas d’encodage pouvait être
éventuellement exploité. Les anciennes versions 3.0.x et 3.1.x de CRS sont
affectées, ainsi que les versions 3.2.1 et 3.3.2 actuellement gérées. Les
intégrateurs et les utilisateurs seraient avisés de mettre à niveau vers les
versions 3.2.2 et 3.3.3 respectivement. La mitigation pour ces vulnérabilités
dépend de l’installation de la dernière version de ModSecurity
(versions 2.9.6/3.0.8).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-39957">CVE-2022-39957</a>

<p>L’ensemble de règles centrales de ModSecurity d’OWASP (CRS) était sujet à un
contournement de corps de réponse. Un client pouvait émettre un champ d’en-tête
Accept HTTP contenant un paramètre facultatif <q>charset</q> afin de recevoir
la réponse dans une forme encodée. Selon le <q>charset</q>, cette réponse
pouvait ne pas être décodée par le pare-feu de l’application web. Une ressource
restreinte, dont l’accès serait ordinairement détecté, pouvait par conséquent
contourner la détection. Les anciennes versions 3.0.x et 3.1.x de CRS sont
affectées, ainsi que les versions 3.2.1 et 3.3.2 actuellement gérées. Les
intégrateurs et les utilisateurs seraient avisés de mettre à niveau vers les
versions 3.2.2 et 3.3.3 respectivement.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-39958">CVE-2022-39958</a>

<p>L’ensemble de règles centrales de ModSecurity d’OWASP (CRS) était sujet à un
contournement de corps de réponse pour exfiltrer séquentiellement de petites et
indétectables sections de données en soumettant de manière répétitive un champ
d’en-tête Range HTTP avec un petit intervalle d’octets. Une ressource restreinte,
dont l’accès serait ordinairement détecté, pourrait être exfiltrée du dorsal,
bien que protégée par le pare-feu de l’application web qui utilise CRS. De
courtes sous-sections de ressource restreinte pouvaient contourner les
techniques de correspondance de modèles et permettre un accès non détecté. Les
anciennes versions 3.0.x et 3.1.x de CRS sont affectées, ainsi que les
versions 3.2.1 et 3.3.2 actuellement gérées. Les intégrateurs et les
utilisateurs seraient avisés de mettre à niveau vers les versions 3.2.2 et 3.3.3
respectivement et de configurer le niveau paranoïa de CRS à 3 ou au-dessus.</p></li>

</ul>

<p>Pour Debian 10 <q>Buster</q>, ces problèmes ont été corrigés dans
la version 3.2.3-0+deb10u3.</p>

<p>Nous vous recommandons de mettre à jour vos paquets modsecurity-crs.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de modsecurity-crs,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/modsecurity-crs">\
https://security-tracker.debian.org/tracker/modsecurity-crs</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3293.data"
# $Id: $
