#use wml::debian::translation-check translation="dd87c2e09c333772ced50302a5b4ef3650fe14da" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Une vulnérabilité a été découverte dans uw-imap, la boîte à outils IMAP de
l’Université de Washington, qui pourrait permettre à des attaquants distants
d’exécuter des commandes arbitraires d’OS si le nom de serveur IMAP est une
entrée non approuvée (par exemple, entrée par un utilisateur de l’application
web) et si rsh a été remplacé par un programme avec des sémantiques d’argument
différentes.</p>

<p>Cette mise à jour désactive l’accès aux boîtes aux lettres IMAP en exécutant
imapd au-dessus de rsh, et par conséquent ssh pour les utilisateurs de
l’application cliente. Le code qui utilise la bibliothèque peut toujours
l’activer avec tcp_parameters() après s’être assuré que le nom du serveur IMAP
a été vérifié.</p>

<p>Pour Debian 8 <q>Jessie</q>, ce problème a été corrigé dans la version 8:2007f~dfsg-4+deb8u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets uw-imap.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1700.data"
# $Id: $
