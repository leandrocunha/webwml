#use wml::debian::translation-check translation="58c7746b887948fe25a0b92d186c4b67440f2367" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Plusieurs problèmes nouvellement référencés ont été corrigés dans le moteur
de fontes FreeType 2.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-9381">CVE-2015-9381</a>

<p>Lecture hors limites de tampon basé sur le tas dans T1_Get_Private_Dict
dans type1/t1parse.c.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-9382">CVE-2015-9382</a>

<p>Lecture hors limites de tampon dans skip_comment dans psaux/psobjs.c à cause
de ps_parser_skip_PS_token mal géré dans une opération FT_New_Memory_Face.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-9383">CVE-2015-9383</a>

<p>Lecture hors limites de tampon basé sur le tas dans tt_cmap14_validate
dans sfnt/ttcmap.c.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 2.5.2-3+deb8u4.</p>
<p>Nous vous recommandons de mettre à jour vos paquets freetype.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1909.data"
# $Id: $
