#use wml::debian::translation-check translation="9d97522df33a7fe5096737f86381a70cc90132dd" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans wireshark, un analyseur de
trafic réseau.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-10894">CVE-2019-10894</a>

<p>Échec d’assertion dans dissect_gssapi_work (paquet-gssapi.c) conduisant à un
plantage du dissecteur GSS-API. Des attaquants distants peuvent exploiter cette
vulnérabilité pour déclencher un déni de service à l'aide d'un paquet contenant
une charge GSS-API contrefaite.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-10895">CVE-2019-10895</a>

<p>Validation insuffisante des données conduisant à un grand nombre de
dépassements d’écritures et de lectures de tampon basé sur le tas dans le module
de traitement de trace de NetScaler (netscaler.c). Des attaquants distants
peuvent exploiter ces vulnérabilités pour déclencher un déni de service, ou tout
autre impact non précisé, à l’aide de paquets contrefaits.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-10899">CVE-2019-10899</a>

<p>Vulnérabilité de lecture hors limites basée sur le tas dans le dissecteur du
protocole Service Location. Des attaquants distants peuvent exploiter ces
vulnérabilités pour déclencher un déni de service, ou tout autre impact non
précisé, à l’aide de paquets SRVLOC contrefaits.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-10901">CVE-2019-10901</a>

<p>Déréférencement de pointeur NULL dans le dissecteur de protocole Local
Download Sharing Service. Des attaquants distants peuvent exploiter ces vulnérabilités
pour déclencher un déni de service à l’aide de paquets LDSS contrefaits.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-10903">CVE-2019-10903</a>

<p>Vérifications manquante de limites conduisant à une vulnérabilité de lecture
hors limites de tas dans le dissecteur du protocole Spool Subsystem de Microsoft.
Des attaquants distants peuvent exploiter ces vulnérabilités pour déclencher un
déni de service, ou tout autre impact non précisé, à l’aide de paquets SPOOLSS
contrefaits.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 1.12.1+g01b65bf-4+deb8u19.</p>
<p>Nous vous recommandons de mettre à jour vos paquets wireshark.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1802.data"
# $Id: $
