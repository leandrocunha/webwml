#use wml::debian::translation-check translation="1d1c1ba842e225bf68a6fed5744786cc779234f7" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Chrony, une implémentation polyvalente du protocole de temps réseau
(Network Time Protocol), ne vérifiait pas les associations de clés
symétriques pour des pairs lors de l'authentification de paquets. Cela
pourrait permettre à des attaquants distants de conduire des attaques
d'usurpation d'identité à l'aide d'une clé de confiance arbitraire, aussi
appelée <q>passe-partout</q>.</p>

<p>Cette mise à jour résout le bogue n° 568492 de Debian.</p>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans la
version 1.24-3.1+deb7u4.</p>

<p>Nous vous recommandons de mettre à jour vos paquets chrony.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-742.data"
# $Id: $
