#use wml::debian::translation-check translation="ce41d997301872adfc27a79ea546429856226b67" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Il est signalé qu'OpenSSH, le client et serveur Secure Shell, avait une
vulnérabilité de déni de service.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-6515">CVE-2016-6515</a>

<p>La fonction d'authentification par mot passe dans sshd d'OpenSSH
antérieur à 7.3 ne limite pas la longueur des mots de passe lors de
l’authentification. Cela permet à des attaquants distants de provoquer un
déni de service (consommation de processeur pour chiffrement) à l'aide de
longues chaînes.</p></li>

</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ce problème a été corrigé dans la
version 6.0p1-4+deb7u6.</p>

<p>Nous vous recommandons de mettre à jour vos paquets openssh.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p></define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-594.data"
# $Id: $
