#use wml::debian::translation-check translation="adc5cbd36ecf754028e80bbdee567a58bca03b81" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>La mise à jour de sécurité publiée sous DLA-993-1 causait une régression pour
quelques applications utilisant Java, incluant jsvc, LibreOffice et Scilab, due
au correctif pour <a href="https://security-tracker.debian.org/tracker/CVE-2017-1000364">CVE-2017-1000364</a>.
Les paquets mis à jour sont maintenant disponibles pour corriger ce problème.
Pour référence, la partie concernée du texte de l’annonce originale suit.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-1000364">CVE-2017-1000364</a>

<p>Qualys Research Labs a découvert que la taille de la page de protection
de pile (« stack guard page ») n'est pas suffisamment grande. Le pointeur de
pile peut franchir la page de protection et passer de la pile à un autre
espace mémoire sans accéder à la page de protection. Dans ce cas aucune
exception de faute de page n'est levée et la pile s'étend dans l'autre
espace de la mémoire. Un attaquant peut exploiter ce défaut pour une
augmentation de droits.</p>

<p>La protection d'espace de pile par défaut est réglée à 256 pages et peut
être configurée sur la ligne de commande du noyau grâce au paramètre
stack_guard_gap du noyau.</p>

<p>Plus de détails peuvent être trouvés sur
<a href="https://www.qualys.com/2017/06/19/stack-clash/stack-clash.txt">https://www.qualys.com/2017/06/19/stack-clash/stack-clash.txt</a></p></li>

</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ce problème a été corrigé dans
la version 3.2.89-2.</p>

<p>Pour Debian 8 <q>Jessie</q>, ce problème a été corrigé dans
la version 3.16.43-2+deb8u2.</p>

<p>Pour Debian 9 <q>Stretch</q>, ce problème a été corrigé dans
la version 4.9.30-2+deb9u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets linux.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-993-2.data"
# $Id: $
