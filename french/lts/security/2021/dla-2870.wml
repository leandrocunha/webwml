#use wml::debian::translation-check translation="8149726db450df3edc0be9bf9d47393a2daca31a" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Apache Log4j2, une infrastructure de journalisation pour Java, est
vulnérable à une attaque par exécution de code distant (RCE) où un
attaquant avec la permission de modifier le fichier de configuration de
journalisation peut construire une configuration malfaisante utilisant un
flux (« appender ») JDBC avec une source de données référençant un URI de
JNDI qui peut exécuter du code distant. Ce problème est corrigé en limitant
les noms de source de données JNDI au protocole Java.</p>

<p>Pour Debian 9 <q>Stretch</q>, ce problème a été corrigé dans la version
2.12.4-0+deb9u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets apache-log4j2.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de apache-log4j2,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/apache-log4j2">\
https://security-tracker.debian.org/tracker/apache-log4j2</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2870.data"
# $Id: $
