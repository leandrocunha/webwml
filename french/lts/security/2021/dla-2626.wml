#use wml::debian::translation-check translation="5409e1a8b16bdd98d3fdc7a40cfc761c28928eba" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Une vulnérabilité dans le module d’analyse de courriel dans le logiciel
Clam AntiVirus version 0.103.1 et toutes les versions précédentes pourrait
permettre à un attaquant distant non authentifié de provoquer une condition
de déni de service sur un périphérique visé. Cette vulnérabilité est due
à une initialisation incorrecte de variable qui pourrait aboutir à une lecture
de pointeur NULL. Un attaquant pourrait exploiter cette vulnérabilité en
envoyant un courriel contrefait à un périphérique visé. Un exploit pourrait
permettre à l’attaquant de provoquer le plantage du processus d’analyse de
ClamAV, aboutissant à une condition de déni de service.</p>

<p>Pour Debian 9 <q>Stretch</q>, ce problème a été corrigé dans
la version 0.102.4+dfsg-0+deb9u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets clamav.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de clamav, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/clamav">\
https://security-tracker.debian.org/tracker/clamav</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2626.data"
# $Id: $
