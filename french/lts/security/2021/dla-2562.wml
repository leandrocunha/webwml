#use wml::debian::translation-check translation="780b89e0dbee08b709eacefc3a8ec54cf6ee7265" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Il a été découvert qu’il existait une vulnérabilité d’exécution de code
à distance dans <a href="https://www.mumble.info/">Mumble</a>, un client VoIP
communément utilisé dans les clavardages de groupe. Son utilisation pouvait
être provoquée par une URL malveillante sur la liste des serveurs.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-27229">CVE-2021-27229</a>

<p>Mumble avant la version 1.3.4 permet l’exécution de code à distance si une
victime parcourait une URL contrefaite sur la liste des serveurs et cliquait sur
le texte Open Webpage.</p></li>

</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans
la version 1.2.18-1+deb9u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets mumble.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2562.data"
# $Id: $
