#use wml::debian::translation-check translation="05427dd47d473ca663a8fcc6e10882d74da12e9c" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs problèmes de sécurité ont été découverts dans le système de base
de données PostgreSQL qui pourraient avoir pour conséquence l'exécution de
code arbitraire ou la divulgation du contenu de la mémoire.</p>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans
la version 9.6.22-0+deb9u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets postgresql-9.6.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de postgresql-9.6, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/postgresql-9.6">\
https://security-tracker.debian.org/tracker/postgresql-9.6</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2662.data"
# $Id: $
