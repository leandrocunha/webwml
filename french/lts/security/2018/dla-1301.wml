#use wml::debian::translation-check translation="ec63203ae7c88b70ad002f6a6d89bd22297d4fb8" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités de sécurité ont été découvertes dans le moteur de
servlet Java Tomcat et le moteur JSP.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-1304">CVE-2018-1304</a>

<p>Le modèle d’URL de "" (chaîne vide), qui correspond exactement au contexte
du superutilisateur, n’était pas géré correctement dans Tomcat d’Apache
lorsqu’il était utilisé comme partie de la définition de restriction. Cela
faisait que la restriction était ignorée. Il était, par conséquent, possible
pour des utilisateurs non autorisés d’acquérir l’accès aux ressources de
l’application web qui auraient dû être protégées. Seules les restrictions de
sécurité avec comme modèle d’URL une chaîne vide sont touchées.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-1305">CVE-2018-1305</a>

<p>Des restrictions de sécurité définies par des annotations de servlet dans
Tomcat d’Apache étaient seulement appliquées lorsqu’un servlet était chargé.
À cause des restrictions de sécurité définies de cette façon et appliquées au modèle
d’URL et de n’importe quelle URL en découlant, il était possible — en fonction
de l’ordre de chargement des servlets — que quelques restrictions de sécurité
ne soient appliquées. Cela pourrait avoir exposé des ressources à des
utilisateurs n’ayant pas de droit d’accès.</p></li>

</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans
la version 7.0.28-4+deb7u18.</p>


<p>Nous vous recommandons de mettre à jour vos paquets tomcat7.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>

</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1301.data"
# $Id: $
