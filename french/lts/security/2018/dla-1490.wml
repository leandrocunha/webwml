#use wml::debian::translation-check translation="1d1c1ba842e225bf68a6fed5744786cc779234f7" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Deux vulnérabilités ont été découvertes dans php5, un langage de script
embarqué dans du HTML et côté serveur. L'un
 (<a href="https://security-tracker.debian.org/tracker/CVE-2018-14851">CVE-2018-14851</a>)
conduit à un déni de service potentiel (lecture hors limites et plantage
d'application) à l'aide d'un fichier JPEG contrefait. L’autre
(<a href="https://security-tracker.debian.org/tracker/CVE-2018-14883">CVE-2018-14883</a>)
est un dépassement d’entier conduisant à une lecture hors limites de tampon basé
sur le tas.</p>

<p>De plus, un correctif précédemment introduit pour
<a href="https://security-tracker.debian.org/tracker/CVE-2017-7272">CVE-2017-7272</a>
a été découvert comme affectant négativement des applications PHP existantes
(n° 890266). En conséquence de ces effets négatifs et le fait que l’équipe de
sécurité ait marqué le CVE en question comme « ignore », le correctif a été
supprimé.</p>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 5.6.37+dfsg-0+deb8u1.</p>
<p>Nous vous recommandons de mettre à jour vos paquets php5.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1490.data"
# $Id: $
