#use wml::debian::translation-check translation="ac3c7b840fb047719cf82feeb8cd6c056a4e90f9" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans le noyau Linux qui
pouvaient conduire à une élévation de privilèges, un déni de service ou des
fuites d'informations.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-0494">CVE-2022-0494</a>

<p>La fonction scsi_ioctl() était vulnérable à une fuite d'informations
uniquement exploitable par les utilisateurs dotés des capacités
CAP_SYS_ADMIN ou CAP_SYS_RAWIO.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-0854">CVE-2022-0854</a>

<p>Ali Haider a découvert une potentielle fuite d'informations dans le
sous-système DMA. Dans les systèmes où la fonction swiotlb est nécessaire,
cela pourrait permettre à un utilisateur local de lire des informations
sensibles.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-1012">CVE-2022-1012</a>

<p>La randomisation lors du calcul des décalages de port dans
l'implémentation d'IP a été améliorée.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-1729">CVE-2022-1729</a>

<p>Norbert Slusarek a découvert une situation de compétition dans le
sous-système perf qui pouvait avoir pour conséquence une élévation locale
de privilèges vers ceux du superutilisateur. Les réglages par défaut dans
Debian évitent son exploitation à moins qu'une configuration plus
permissive ait été appliquée dans le sysctl kernel.perf_event_paranoid.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-1786">CVE-2022-1786</a>

<p>Kyle Zeng a découvert une utilisation de mémoire après libération dans
le sous-système io_uring qui peut avoir pour conséquence une élévation
locale de privilèges vers ceux du superutilisateur.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-1789">CVE-2022-1789</a> /
<a href="https://security-tracker.debian.org/tracker/CVE-2022-1852">CVE-2022-1852</a>

<p>Yongkang Jia, Gaoning Pan et Qiuhao Li ont découvert deux
déréférencements de pointeur NULL dans le traitement des instructions de
processeur de KVM, ayant pour conséquence un déni de service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-32250">CVE-2022-32250</a>

<p>Aaron Adams a découvert une utilisation de mémoire après libération dans
Netfilter qui peut avoir pour conséquence une élévation locale de
privilèges vers ceux du superutilisateur.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-1974">CVE-2022-1974</a> /
<a href="https://security-tracker.debian.org/tracker/CVE-2022-1975">CVE-2022-1975</a>

<p>Duoming Zhou a découvert que l'interface Netlink NFC était vulnérable à
un déni de service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-2078">CVE-2022-2078</a>

<p>Ziming Zhang a découvert une écriture hors limites dans Netfilter qui
peut avoir pour conséquence une élévation locale de privilèges vers ceux du
superutilisateur.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-21499">CVE-2022-21499</a>

<p>Le débogueur du noyau pouvait être utilisé pour contourner les
restrictions de l'amorçage sécurisé avec UEFI (« UEFI secure boot »).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-28893">CVE-2022-28893</a>

<p>Felix Fu a découvert une utilisation de mémoire après libération dans
l'implémentation du protocole Remote Procedure Call (SunRPC), qui pouvait
avoir pour conséquences un déni de service ou une fuite d'informations.</p></li>

</ul>

<p>Pour la distribution stable (Bullseye), ces problèmes ont été corrigés
dans la version 5.10.120-1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets linux.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de linux, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/linux">\
https://security-tracker.debian.org/tracker/linux</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2022/dsa-5161.data"
# $Id: $
