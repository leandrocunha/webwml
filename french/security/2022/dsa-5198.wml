#use wml::debian::translation-check translation="d3c36081141f159ff0a43ff0c5bf3e0e2f57fde3" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Deux vulnérabilités de sécurité ont été découvertes dans Jetty, un moteur
de servlet Java et serveur web.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-2047">CVE-2022-2047</a>

<p>Dans Eclipse Jetty, l'analyse du segment <q>authority</q> d'un schéma
d'URI HTTP, la classe HttpURI de Jetty, détecte incorrectement une entrée
non valable comme nom d'hôte. Cela peut conduire à des échecs dans un
scénario de mandataire.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-2048">CVE-2022-2048</a>

<p>Dans l'implémentation du serveur HTTP/2 Eclipse Jetty, lors de la
rencontre d'une requête HTTP/2 non valable, le traitement de l'erreur
contient un bogue qui peut finir par ne pas nettoyer correctement les
connexions actives et les ressources associées. Cela peut conduire à un
scénario de déni de service où il reste insuffisamment de ressources pour
traiter les requêtes correctes.</p></li>

</ul>

<p>Pour la distribution stable (Bullseye), ces problèmes ont été corrigés
dans la version 9.4.39-3+deb11u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets jetty9.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de jetty9, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/jetty9">\
https://security-tracker.debian.org/tracker/jetty9</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2022/dsa-5198.data"
# $Id: $
