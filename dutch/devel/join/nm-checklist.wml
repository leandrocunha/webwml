#use wml::debian::template title="Checklist bij kandidaturen"
#include "$(ENGLISHDIR)/devel/join/nm-steps.inc"
#use wml::debian::translation-check translation="517d7a4f999561b8f28e207214a40990fdc3da49"

<p>De informatie op deze pagina, hoewel openbaar, zal voornamelijk van belang
zijn voor toekomstige Debian-ontwikkelaars.</p>

<h3>0. Bijdragen leveren</h3>

Voordat u besluit om u aan te melden als kandidaat, moet u al bijdragen aan
Debian door pakketten, vertalingen, documentatie of enige andere activiteit te
leveren. Meestal bent u al geregistreerd als <a
href="https://wiki.debian.org/DebianMaintainer">onderhouder van Debian</a> en
uploadt u al zes maanden uw pakketten.

<p>Het huidige registratieproces voor nieuwe leden is opgedeeld in vier
stappen:</p>

<h3>1. Kandidaatstelling en aanbeveling</h3>
<p>Normaal gesproken wordt het proces gestart met een aanvraag via de
<a href="https://nm.debian.org/public/newnm">webinterface voor nieuwe
kandidaat-leden</a>.
<br>
Na deze stap is de kandidaat bekend in het systeem en is de Debian balie voor
nieuwe leden het primaire aanspreekpunt voor vragen over de kandidatuur. De
balie beheert of houdt toezicht op alle andere stappen en zal proberen te
helpen met alle problemen die zich voordoen.
</p>

<p>Nadat de aanvraag is ingediend bij het systeem, wordt deze bepleit door een
officiële Debian-ontwikkelaar die met de kandidaat heeft samengewerkt. Dit
wordt gedaan door middel van een ondertekende e-mail, met daarin een korte
tekst over de kandidaat. Normaal gesproken behandelt deze e-mail onderwerpen
zoals de achtergrond van de kandidaat, wat hij/zij voor Debian heeft gedaan en
een paar elementen in verband met toekomstplannen.</p>

<p>Vervolgens kijkt de balie naar wat de kandidaat al heeft gedaan in het
project. Om het proces voor nieuwe leden zo efficiënt mogelijk te laten
verlopen, zouden kandidaten al een significante bijdrage aan Debian moeten
hebben geleverd. Dit kan zijn: verpakken, documentatie,
kwaliteitsbewaking, ...</p>

<p><a href="./nm-step1">Meer ...</a></p>

<h3>2. Controles door een kandidatuurbeheerder</h3>
<p>Zodra er een kandidatuurbeheerder beschikbaar is, wijst de balie er een toe
aan de kandidaat. De taak van de kandidatuurbeheerder is het verzamelen van de
informatie die nodig is om een basis te vormen voor de beslissing van de Debian
accountbeheerder. Dit is opgedeeld in 4 delen:</p>

<h4>ID-controle</h4>
<p>Om het sterke web van vertrouwen te behouden dat alle Debian-ontwikkelaars
met elkaar verbindt, moeten kandidaten zich identificeren door een
OpenPGP-sleutel te verstrekken die is ondertekend door ten minste twee
officiële ontwikkelaars. Om hun identiteit verder te verzekeren, worden
handtekeningen door andere mensen (die geen ontwikkelaars van Debian hoeven te
zijn, maar goed verbonden moeten zijn met het algehele web van vertrouwen)
sterk aanbevolen.</p>

<p><a href="./nm-step2">Meer ...</a></p>

<h4>Controle op overtuiging en procedures</h4>
<p>Aangezien Debian bekend staat om zijn sterke ethische en filosofische
achtergrond, moeten kandidaten hun eigen mening over het onderwerp Vrije
Software toelichten. Debian heeft ook behoorlijk gecompliceerde
standaardprocedures ontwikkeld om de problemen van het werken in een grote
groep aan te pakken, dus moeten kandidaten aantonen dat ze deze kennen en in
staat zijn ze toe te passen op concrete situaties.</p>

<p><a href="./nm-step3">Meer ...</a></p>

<h4>Controle van taken en vaardigheden</h4>
<p>Om de algehele kwaliteit van de Debian-distributie te waarborgen, moeten
kandidaten laten zien dat zij hun taken kennen in het domein waarop zij willen
werken (documentatie en internationalisering of pakketonderhoud).
#We have more areas, I'm sure of that - but I can't remember them now: FIXME
Zij moeten ook hun vaardigheden tonen door voorbeelden van hun werk voor te
leggen en enkele vragen daarover te beantwoorden.</p>

<p><a href="./nm-step4">Meer ...</a></p>

<h4>Aanbeveling</h4>
<p>Als de kandidatuurbeheerder tevreden is over de prestaties van de
kandidaat, wordt een kandidatuurrapport opgesteld en ingediend bij de
balie en de accountbeheerders van Debian.</p>

<p><a href="./nm-step5">Meer ...</a></p>

<h3>3. Controle door de balie</h3>
<p>Het kandidatuurrapport wordt door een lid van de balie van Debian
gecontroleerd op formele problemen. Als deze ernstig zijn, wordt het rapport
afgewezen en moet de kandidatuurbeheerder het probleem oplossen. Als er slechts
kleine fouten zijn, worden deze gemeld aan de kandidaat en de
kandidatuurbeheerder.</p>

<p><a href="./nm-step6">Meer ...</a></p>

<h3>4. Controle door de Debian accountbeheerder en aanmaken van een account</h3>
<p>In deze laatste fase van het proces wordt het kandidatuurrapport beoordeeld.
Indien nodig worden verdere controles uitgevoerd door de accountbeheerder of
wordt een kandidatuurbeheerder gevraagd een nieuwe controle uit te voeren. Soms
is een telefoontje nodig om de aanvraag af te ronden.</p>

<p>Als alle mogelijke problemen zijn opgelost, wijzen de
Debian-accountbeheerders een account toe aan de kandidaat en voegen ze diens
OpenPGP-sleutel toe aan de Debian-sleutelbos.</p>

<p><a href="./nm-step7">Meer ...</a></p>
