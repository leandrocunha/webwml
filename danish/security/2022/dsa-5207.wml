#use wml::debian::translation-check translation="0413fa50844efcfa46bf7f8f7809d825fd42fffa" mindelta="1"
<define-tag description>sikkerhedsopdatering</define-tag>
<define-tag moreinfo>
<p>Flere sårbarheder er opdaget i Linux-kernen, hvilke kunne føre til 
rettighedsforøgelse, lammelsesangreb eller informationslækager.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-2585">CVE-2022-2585</a>

    <p>En fejl i forbindelse med anvendelse efter frigivelse i implementeringen 
    af POSIX CPU-timere, kunne medføre lammelsesangreb eller lokal 
    rettighedsforøgelse.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-2586">CVE-2022-2586</a>

    <p>En anvendelse efter frigivelse i undersystemet Netfilter, kunne medføre 
    lokal rettighedsforøgelse for en bruger med kapabiliteten CAP_NET_ADMIN i 
    ethvert bruger- eller netværksnavnerum.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-2588">CVE-2022-2588</a>

    <p>Zhenpeng Lin opdagede en fejl i forbindelse med anvendelse efter 
    frigivelse i implementeringen af filteret cls_route, hvilken kunne medføre 
    lokal rettighedsforøgelse for en bruger med kapabiliteten CAP_NET_ADMIN i 
    ethvert bruger- eller netværksnavnerum.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-26373">CVE-2022-26373</a>

    <p>Man opdagede at visse processorer med Intels Enhanced Indirect Branch 
    Restricted Speculation-muligheder (eIBRS), som er undtagelser til 
    dokumenterede egenskaber i nogle situationer, hvilket kunne medføre 
    informationsafsløring.</p>

    <p>Intels beskrivelse af problemet, kan findes på 
    <url "https://www.intel.com/content/www/us/en/developer/articles/technical/software-security-guidance/advisory-guidance/post-barrier-return-stack-buffer-predictions.html"></p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-29900">CVE-2022-29900</a>

    <p>Johannes Wikner og Kaveh Razavi rapporterede at for 
    AMD/Hygon-processorer, kunne fejltrænede forgreningsforudsigelser til 
    returinstruktioner, kunne muliggøre vilkårlig spekulativ kodeudførelse under 
    visse mikroarkitekturafhængige betingelser.</p>

    <p>En liste over påvirkede AMD CPU-typer, kan findes på 
    <url "https://www.amd.com/en/corporate/product-security/bulletin/amd-sb-1037"></p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-29901">CVE-2022-29901</a>

    <p>Johannes Wikner og Kaveh Razavi rapporterede at for Intel-processorer 
    (Intel Core generation 6, 7 og 8), var beskyttelsen mod indsprøjtningsangreb 
    i forbindelse med spekulative forgreningsmål, utilstrækkelig under nogle 
    omstændigheder, hvilken kunne muliggøre vilkårlig, spekulativ udførelse af 
    vilkårlig kode under visse mikroarkitekturafhængige betingelser.</p>

    <p>Flere oplysninger finder man på 
    <url "https://www.intel.com/content/www/us/en/developer/articles/technical/software-security-guidance/advisory-guidance/return-stack-buffer-underflow.html"></p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-36879">CVE-2022-36879</a>

    <p>En fejl blev opdaget i xfrm_expand_policies i undersystemet xfrm, hvilken 
    kunne forårsage at en referenceoptælling kunne blive smidt væk to 
    gange.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-36946">CVE-2022-36946</a>

    <p>Domingo Dirutigliano og Nicola Guerrera rapporterede om en 
    hukommelseskorruptionsfejl i undersystemet Netfilter, hvilken kunne medføre 
    lammelsesangreb.</p></li>

</ul>

<p>I den stabile distribution (bullseye), er disse problemer rettet i
version 5.10.136-1.</p>

<p>Vi anbefaler at du opgraderer dine linux-pakker.</p>

<p>For detaljeret sikkerhedsstatus vedrørende linux, se
dens sikkerhedssporingssidede på:
<a href="https://security-tracker.debian.org/tracker/linux">\
https://security-tracker.debian.org/tracker/linux</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2022/dsa-5207.data"
