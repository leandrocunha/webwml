#use wml::debian::translation-check translation="14d91c5e8b991574bd42901de0e30d4eb4a8bb7e" mindelta="1"
<define-tag description>sikkerhedsopdatering</define-tag>
<define-tag moreinfo>
<p>Flere sårbarheder er opdaget i Linux-kernen, hvilke kunne føre til en 
rettighedsforøgelse, lammelsesangreb eller informationlækager.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-43976">CVE-2021-43976</a>

    <p>Zekun Shen og Brendan Dolan-Gavitt opdagede en fejl i funktionen 
    mwifiex_usb_recv() i USB-driveren Marvell WiFi-Ex.  En angriber, der er i 
    stand til at tilslutte en fabrikeret USB-enhed, kunne drage nytte af fejlen 
    til at forårsage et lammelsesangreb.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-0330">CVE-2022-0330</a>

    <p>Sushma Venkatesh Reddy opdagede en manglende GPU TLB-flush i 
    i915-driveren, medførende lammelsesangreb eller 
    rettighedsforøgelse.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-0435">CVE-2022-0435</a>

    <p>Samuel Page og Eric Dumazet rapporteret om et stakoverløb i 
    netværksmodulet til protokollen Transparent Inter-Process Communication 
    (TIPC), medførende lammelsesangreb eller potentielt udførelse af vilkårlig 
    kode.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-0516">CVE-2022-0516</a>

    <p>Man opdagede at et utilstrækkeligt tjek i KVM-undersystemet til s390x, 
    kunne muliggøre uautoriseret læse- og skriveadgang til hukommelse.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-0847">CVE-2022-0847</a>

    <p>Max Kellermann opdagede en fejl i håndteringen af pipebufferflag.  En 
    angriber kunne drage nytte af fejlen til lokal rettighedsforøgelse.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-22942">CVE-2022-22942</a>

    <p>Man opdagede at forkert håndtering af fildescriptorer i driveren VMware 
    Virtual GPU (vmwgfx), kunne medføre informationslækage eller 
    rettighedsforøgelse.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-24448">CVE-2022-24448</a>

    <p>Lyu Tao rapporterede om en fejl i NFS-implementeringen i Linux-kernen, 
    når der håndteres forespørgsler til at åbne en mappe til en almindelig fil, 
    hvilket kunne medføre en informationslækage.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-24959">CVE-2022-24959</a>

    <p>En hukommelseslækage blev opdaget i funktionen yam_siocdevprivate() i 
    YAM-driveren til AX.25, hvilken kunne medføre lammelsesangreb.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-25258">CVE-2022-25258</a>

    <p>Szymon Heidrich rapporterede at USB Gadget-undersystemet manglede visse 
    valideringer af descriptorforespørgsler til grænseflade-OS, medførende 
    hukommelseskorruption.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-25375">CVE-2022-25375</a>

    <p>Szymon Heidrich rapporterede at RNDIS USB-gadget manglende validering af 
    størrelsen på kommandoen RNDIS_MSG_SET, medførende informationslækage fra 
    kernehukommelse.</p></li>

</ul>

<p>I den stabile distribution (bullseye), er disse problemer rettet i
version 5.10.92-2.</p>

<p>Vi anbefaler at du opgraderer dine linux-pakker.</p>

<p>For detaljeret sikkerhedsstatus vedrørende linux, se
dens sikkerhedssporingssidede på:
<a href="https://security-tracker.debian.org/tracker/linux">\
https://security-tracker.debian.org/tracker/linux</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2022/dsa-5092.data"
