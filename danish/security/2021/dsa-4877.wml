#use wml::debian::translation-check translation="9b6e2575a6c497232661fc247cef61ab70393208" mindelta="1"
<define-tag description>sikkerhedsopdatering</define-tag>
<define-tag moreinfo>
<p>Følgende sårbarheder er opdaget i webmotoren webkit2gtk:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-27918">CVE-2020-27918</a>

    <p>Liu Long opdagede at behandling af ondsindet fremstillet webindhold 
    kunne føre til udførelse af vilkårlig kode.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-29623">CVE-2020-29623</a>

    <p>Simon Hunt opdagede at brugere kunne være forhindret i fuldstændigt at 
    slette deres browserhistorik under visse omstændigheder.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-1765">CVE-2021-1765</a>

    <p>Eliya Stein opdagede at ondsindet fremstillet webindhold kunne overtræde 
    iframe-sandkasse-policy'en.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-1789">CVE-2021-1789</a>

    <p>@S0rryMybad opdagede at behandling af ondsindet fremstillet webindhold 
    kunne føre til udførelse af vilkårlig kode.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-1799">CVE-2021-1799</a>

    <p>Gregory Vishnepolsky, Ben Seri og Samy Kamkar opdagede at et ondsindet 
    websted kunne være i stand til at tilgå begrænsede porte på vilkårlige 
    servere.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-1801">CVE-2021-1801</a>

    <p>Eliya Stein opdagede at behandling af ondsindet fremstillet webindhold 
    kunne føre til udførelse af vilkårlig kode.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-1870">CVE-2021-1870</a>

    <p>En anonym efterforsker opdagede at behandling af ondsindet fremstillet 
    webindhold kunne føre til udførelse af vilkårlig kode.</p></li>

</ul>

<p>I den stabile distribution (buster), er disse problemer rettet i
version 2.30.6-1~deb10u1.</p>

<p>Vi anbefaler at du opgraderer dine webkit2gtk-pakker.</p>

<p>For detaljeret sikkerhedsstatus vedrørende webkit2gtk, se
dens sikkerhedssporingsside på:
<a href="https://security-tracker.debian.org/tracker/webkit2gtk">\
https://security-tracker.debian.org/tracker/webkit2gtk</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2021/dsa-4877.data"
