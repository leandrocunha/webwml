#use wml::debian::template title="Уязвимости GRUB2 UEFI SecureBoot &mdash; 2021"
#use wml::debian::translation-check translation="eaa2e2477454c72064e63ad9f58a59ec94b9d105" maintainer="Lev Lamberov"

<p>
После анонса в июле 2020 г.
о <a href="$(HOME)/security/2020-GRUB-UEFI-SecureBoot">"BootHole"</a>,
группе ошибок в GRUB2, исследователи безопасности
и разработчики Debian, а также других дистрибутивов продолжили поиск
других проблем, которые могли бы позволить обойти UEFI Secure
Boot. Было обнаружено ещё несколько таких ошибок. Подробности см. в
<a href="$(HOME)/security/2021/dsa-4867">рекомендации по безопасности Debian
4867-1</a>. Цель данного документа состоит в том, чтобы объяснить
последствия этих уязвимостей, а также указать, что следует предпринять для
того, чтобы исправить их. </p>

<ul>
  <li><b><a href="#what_is_SB">Общая информация: что такое UEFI Secure Boot?</a></b></li>
  <li><b><a href="#grub_bugs">Обнаружение многочисленных ошибок GRUB2</a></b></li>
  <li><b><a href="#revocations">Необходимость отзыва ключей для исправления цепочки Secure Boot</a></b></li>
  <li><b><a href="#revocation_problem">Каковы последствия отзыва ключей?</a></b></li>
  <li><b><a href="#package_updates">Обновлённые пакеты и ключи</a></b>
  <ul>
    <li><b><a href="#grub_updates">1. GRUB2</a></b></li>
    <li><b><a href="#linux_updates">2. Linux</a></b></li>
    <li><b><a href="#shim_updates">3. Shim и SBAT</a></b></li>
    <li><b><a href="#fwupdate_updates">4. Fwupdate</a></b></li>
    <li><b><a href="#fwupd_updates">5. Fwupd</a></b></li>
    <li><b><a href="#key_updates">6. Ключи</a></b></li>
  </ul></li>
  <li><b><a href="#buster_point_release">Редакция Debian 10.10 (<q>buster</q>),
        обновлённые установочные носители и <q>живые</q>
        образы</a></b></li>
  <li><b><a href="#more_info">Дополнительная информация</a></b></li>
</ul>

<h1><a name="what_is_SB">Общая информация: что такое UEFI Secure Boot?</a></h1>

<p>
UEFI Secure Boot (SB) представляет собой механизм проверки, гарантирующий что
запускаемый UEFI-микропрограммой компьютера код является доверенным. Он разработан
для защиты системы от вредоносного кода, загружаемого и исполняемого
достаточно рано в процессе старта и до загрузки операционной
системы.
</p>

<p>
SB работает с использованием криптографических контрольных сумм и подписей. Каждая
программа, загружаемая микропрограммой, содержит подпись и контрольную сумму.
Перед тем, как разрешить исполнение, микропрограмма выполняет проверку того, что
запускаемая программа является доверенной. Это осуществляется путём проверки контрольной
суммы и подписи. Если SB включён в системе, то любая попытка выполнить
недоверенную программу будет запрещена. Это позволяет предотвратить
запуск непредусмотренного / неавторизованного кода в UEFI-окружении.
</p>

<p>
Большинство оборудования с архитектурой x86 поставляется с завода с предустановленными
ключами Microsoft. Это означает, что микропрограмма в такой системе доверяет двоичным
файлам, подписанным Microsoft. Большинство современных систем поставляются с включённым
механизмом SB, по умолчанию они не запускают какой-либо неподписанный код. Однако
можно изменить настройки микропрограммы и либо отключить SB, либо
зарегистрировать дополнительные ключи.
</p>

<p>
Debian, как и многие другие операционные системы на основе Linux, использует программу,
называемую shim, для расширения доверия от микропрограммы до других программ,
которые нам требуется обезопасить в ходе ранней загрузки &mdash; это загрузчик GRUB2,
ядро Linux и инструменты обновления микропрограмм (fwupd и
fwupdate).
</p>

<h1><a name="grub_bugs">Обнаружение многочисленных ошибок GRUB2</a></h1>

<p>
В модуле <q>acpi</q> для GRUB2 была обнаружена ошибка. Этот модуль
разработан для предоставления интерфейса драйверов для ACPI (<q>улучшенного
интерфейса для конфигурации и управления электропитанием</q>), довольно распространённой
части современного оборудования. К сожалению, модуль ACPI в настоящее время
позволяет привилегированному пользователю загружать специально сформированные таблицы ACPI
при включённом Secure Boot и выполнять произвольные изменения состояния системы;
это позволяет людям легко ломать цепочку Secure Boot. В настоящее время эта
уязвимость исправлена.
</p>

<p>
Как и в случае с BootHole вместо исправления этой одной ошибки разработчики
продолжили более глубокий аудит и анализ исходного кода GRUB2.
Было бы безответственно исправить одну серьёзную уязвимость без поиска
других уязвимостей! Мы обнаружили ещё несколько мест в коде, где выделение
внутренней памяти может приводить к переполнению при получении неожиданных входных
данных, а также несколько мест, где память может использоваться после её освобождения.
Исправления для всех этих ошибок были распространены среди участников сообщества и
протестированы.
</p>

<p>
Полный список обнаруженных ошибок можно найти в <a href="$(HOME)/security/2021/dsa-4867">рекомендации
по безопасности Debian 4867-1</a>.
</p>


<h1><a name="revocations">Необходимость отзыва ключей для исправления цепочки Secure Boot</a></h1>

<p>
Очевидно, Debian и другие поставщики операционных систем <a
href="#package_updates">выпустят исправленные версии</a> GRUB2 и
Linux. Тем не менее на этом исправление проблем не завершается.
Злоумышленники всё ещё могут быть способны использовать предыдущие уязвимые
версии загрузчика и ядра, чтобы обойти Secure Boot.
</p>

<p>
Для предотвращения этого следующим шагом будет внесение сотрудниками Microsoft небезопасных
двоичных файлов в список блокировки, чтобы они не могли быть запущены при включённом SB.
Это достигается с помощью списка <b>DBX</b>, который является частью UEFI Secure Boot.
Все дистрибутивы Linux, поставляющие подписанные Microsoft копии shim,
должны предоставить подробную информацию о двоичных файлах или используемых ключах,
чтобы указанные действия были выполнены. Будет обновлён <a
href="https://uefi.org/revocationlistfile">UEFI-файл со списком отозванных ключей</a>,
обновление будет содержать предоставленную информацию. Позднее, <b>некоторый</b>
момент времени, системы начнут использовать этот обновлённый список и перестанут
запускать уязвимые двоичные файлы при использовании Secure Boot.
</p>

<p>
<i>Точный</i> срок развёртывания этого изменения пока не ясен.
В какой-то момент поставщики BIOS/UEFI добавят новый список отозванных ключей в новые сборки
микропрограмм для нового оборудования. Microsoft <b>может</b>
выпустить обновления для существующих систем через Windows Update. Некоторые
дистрибутивы Linux также могут выпустить обновления через свои собственные
системы обновления безопасности. Debian <b>пока</b> этого не сделал, но мы
собираемся сделать это в будущем.
</p>

<h1><a name="revocation_problem">Каковы последствия отзыва ключей?</a></h1>

<p>
Большинство поставщиков с недоверием относятся к автоматическому применению
обновлений, которые отзывают ключи, используемые для Secure Boot. Существующие
наборы ПО могут неожиданно перестать загружаться при включённом SB, если
пользователь также не установил требуемые обновления ПО. Двойная
загрузка систем Windows/Linux тоже может неожиданно прекратить загрузку
Linux. Конечно же, старые установочные образы и <q>живые</q> тоже перестанут
загружаться, что потенциально усложнит восстановление систем.
</p>

<p>
Имеются два очевидных способа исправления незагружающейся системы:
</p>

<ul>
  <li>Перезагрузиться в режиме <q>восстановления</q>,
    используя <a href="#buster_point_release">более новые установочные носители</a>,
    и применить необходимые обновления; или</li>
  <li>Временно отключить Secure Boot для получения доступа к системе,
    применить обновления, а затем заново включить SB.</li>
</ul>

<p>
Оба пути могут вначале показаться простыми, однако каждый из них потребует
некоторого количества времени, особенно от пользователей нескольких систем.
Кроме того, помните, что для включения или отключения
Secure Boot требуется непосредственный доступ к машине. Обычно
<b>нельзя</b> изменить эту настройку извне системы настройки микропрограммы
компьютера. Удалённые серверные машины могут потребовать специального
обращения по этой самой причине.
</p>

<p>
По этим причинам настоятельно рекомендуется, чтобы <b>все</b> пользователи
Debian установили все <a href="#package_updates">рекомендованные обновления</a> для
своих систем как можно скорее. Это позволит снизить вероятность возникновения
проблем в будущем.
</p>

<h1><a name="package_updates">Обновлённые пакеты</a></h1>

<p>
<b>ВНИМАНИЕ:</b> системы, использующие Debian 9 (<q>stretch</q>) и более ранние
выпуски <b>необязательно</b> получат соответствующие обновления, поскольку Debian 10
(<q>buster</q>) является первым выпуском Debian, включающим поддержку UEFI
Secure Boot.
</p>

<p>
В Debian имеются пять пакетов с исходным кодом, которые будут обновлены из-за
описанных здесь изменений UEFI Secure Boot:
</p>

<h2><a name="grub_updates">1. GRUB2</a></h2>

<p>
Обновлённые версии Debian-пакетов GRUB2 доступны уже сейчас в архиве
debian-security для стабильного выпуска Debian 10
(<q>buster</q>). Исправленные версии очень скоро появятся в обычном архиве Debian
для разрабатываемых версий Debian (нестабильного и тестируемого).
</p>

<h2><a name="linux_updates">2. Linux</a></h2>

<p>
Обновлённые версии Debian-пакетов linux будут доступны в скором времени через
buster-proposed-updates для стабильного выпуска Debian 10 (<q>buster</q>)
и будут включены в готовящуюся редакцию 10.10. Новые пакеты
также скоро появятся в архиве Debian для разрабатываемых версий
Debian (нестабильного и тестируемого). Мы надеемся, что исправленные пакеты
будут вскоре загружены в buster-backports.
</p>

<h2><a name="shim_updates">3. Shim и SBAT</a></h2>

<p>
Обнаружение серии ошибок <q>BootHole</q> было первым случаем, когда в
экосистеме UEFI Secure Boot потребовалось отозвать ключи. Это показало
досадную ошибку проектирования в механизме отзыва ключей SB: большое
количество разных дистрибутивов Linux и разных двоичных файлов UEFI приводят
к тому, что список отзыва ключей растёт очень быстро. Многие компьютерные системы
имеют лишь ограниченный объём места для хранения данных об отзыве ключей,
отзыв большого числа ключей может быстро заполнить этот объём и привести
к различным поломкам систем.
</p>

<p>
Для борьбы с этой проблемой разработчики shim разработали более эффективный в
плане места для хранения метод блокировки в будущем небезопасных двоичных файлов
UEFI. Он называется <b>SBAT</b> (<q>Secure Boot Advanced Targeting</q>).
Он работает путём отслеживания номеров поколения подписанных программ.
Вместо отзыва отдельных подписей при обнаружении проблем используются
счётчики для определения старых версий программ, которые более не считаются
безопасными. Отзыв старых серий двоичных файлов GRUB2 (например)
теперь потребует обновления переменной UEFI, содержащей номер поколения
для GRUB2; любые версии GRUB2 с более старым номером более не будут считаться
безопасными. За дополнительной информацией о SBAT обращайтесь к
shim <a href="https://github.com/rhboot/shim/blob/main/SBAT.md">документации
по SBAT</a>.
</p>

<p>
<b>К сожалению</b>, новая разработка shim SBAT ещё не совсем готова.
Разработчики собирались сейчас выпустить новую версию shim, содержащую
эту новую возможность, но они столкнулись с неожиданными проблемами.
Разработка ещё продолжается. Мы ожидаем, что сообщество Linux очень скоро
обновиться до этой новой версии shim. До того, как эта разработка
будет готова, мы все продолжим использовать наши существующие подписанные
двоичные файлы shim.
</p>

<p>
Обновлённые версии Debian-пакетов shim будут доступны сразу же,
как только работа будет завершена. О них будет объявлено здесь
и в других местах. Кроме того, мы опубликуем новую редакцию Debian,
10.10, а также выпустим новые пакеты shim для разрабатываемых версий
Debian (нестабильного и тестируемого).
</p>

<h2><a name="fwupdate_updates">4. Fwupdate</a></h2>

<p>
Обновлённые версии Debian-пакетов fwupdate будут доступны в скором времени
через buster-proposed-updates для стабильного выпуска Debian 10 (<q>buster</q>)
и будут включены в готовящуюся редакцию 10.10. Пакет fwupdate уже был
удалён из нестабильного и тестируемого выпусков в связи с его заменой
на пакет fwupd.
</p>

<h2><a name="fwupd_updates">5. Fwupd</a></h2>

<p>
Обновлённые версии Debian-пакетов fwupd будут доступны в скором времени
через buster-proposed-updates для стабильного выпуска Debian 10 (<q>buster</q>)
и будут включены в готовящуюся редакцию 10.10. Новые пакеты
также находятся в архиве Debian для разрабатываемых версий
Debian (нестабильного и тестируемого).
</p>

<h2><a name="key_updates">6. Ключи</a></h2>

<p>
Разработчики Debian создали новые ключи для подписывания и сертификаты для своих
пакетов Secure Boot. Ранее мы использовали один сертификат для всех наших пакетов:
</p>

<ul>
  <li>Debian Secure Boot Signer 2020
  <ul>
    <li>(fingerprint <code>3a91a54f9f46a720fe5bbd2390538ba557da0c2ed5286f5351fe04fff254ec31)</code></li>
    </ul></li>
</ul>

<p>
Теперь мы перешли на использование отдельных ключей и сертификатов для каждого из
пяти разных пакетов с исходным кодом, что в будущем даст нам большую
гибкость:
</p>

<ul>
  <li>Debian Secure Boot Signer 2021 - fwupd
  <ul>
    <li>(fingerprint <code>309cf4b37d11af9dbf988b17dfa856443118a41395d094fa7acfe37bcd690e33</code>)</li>
  </ul></li>
  <li>Debian Secure Boot Signer 2021 - fwupdate
  <ul>
    <li>(fingerprint <code>e3bd875aaac396020a1eb2a7e6e185dd4868fdf7e5d69b974215bd24cab04b5d</code>)</li>
  </ul></li>
  <li>Debian Secure Boot Signer 2021 - grub2
  <ul>
    <li>(fingerprint <code>0ec31f19134e46a4ef928bd5f0c60ee52f6f817011b5880cb6c8ac953c23510c</code>)</li>
  </ul></li>
  <li>Debian Secure Boot Signer 2021 - linux
  <ul>
    <li>(fingerprint <code>88ce3137175e3840b74356a8c3cae4bdd4af1b557a7367f6704ed8c2bd1fbf1d</code>)</li>
  </ul></li>
  <li>Debian Secure Boot Signer 2021 - shim
  <ul>
    <li>(fingerprint <code>40eced276ab0a64fc369db1900bd15536a1fb7d6cc0969a0ea7c7594bb0b85e2</code>)</li>
  </ul></li>
</ul>

<h1><a name="buster_point_release">Редакция Debian 10.10 (<q>buster</q>)
обновлённые установочные носители и <q>живые</q> образы</a></h1>

<p>
Все описанные здесь обновления планируется включить в редакцию
Debian 10.10 (<q>buster</q>), который будет выпущен в скором времени.
Таким образом, пользователям, которым нужны установочные и <q>живые</q>
образы Debian следует выбирать 10.10. В будущем более ранние образы могут
не работать с Secure Boot, так как будет выполнен отзыв ключей.
</p>

<h1><a name="more_info">Дополнительная информация</a></h1>

<p>
Дополнительную информацию о настройке UEFI Secure Boot в Debian
можно найти в вики Debian по адресу <a href="https://wiki.debian.org/SecureBoot">\
https://wiki.debian.org/SecureBoot</a>.</p>

<p>
Другие ресурсы по данной теме:
</p>

<ul>
  <li><a href="https://access.redhat.com/security/vulnerabilities/RHSB-2021-003">Статья
  об уязвимости от Red Hat</a></li>
  <li><a href="https://www.suse.com/support/kb/doc/?id=000019892">Статья
  об уязвимости от SUSE</a></li>
  <li><a href="https://wiki.ubuntu.com/SecurityTeam/KnowledgeBase/GRUB2SecureBootBypass2021">Статья
  по безопасности от Ubuntu</a></li>
</ul>
