<define-tag pagetitle>Besitz der Domain <q>debian.community</q></define-tag>
<define-tag release_date>2022-08-07</define-tag>
#use wml::debian::news
#use wml::debian::translation-check translation="7dcdd3e690d3084ffd92cb099c4321e35edccfc1" maintainer="Erik Pfannenstein"

<p>
Die World Intellectual Property Organization (Weltorganisation für geistiges Eigentum, WIPO) hat seiner 
Uniform Domain-Name Dispute-Resolution Policy (Richtlinie zur Auflösung von Disputen über einheitliche Domain-Namen, UDRP) 
folgend beschlossen, dass der Besitz der Domain <q><a href="https://debian.community">debian.community</a></q> an das 
<a href="https://www.wipo.int/amc/en/domains/search/case.jsp?case=D2022-1524">Debian-Projekt übertragen werden soll.</a>
</p>

<p>
Das einberufene Gremium stellte fest, dass <q>der angefochtene Domain-Name mit einem Markenzeichen, 
zu dem der Beschwerdeführer die Markenrechte besitzt, übereinstimmt.</q>
</p>

<p>
In seiner Entscheidung führt das Gremium aus:
<p>

<blockquote>
[...] der angefochtene Domain-Name ist identisch zum DEBIAN-Markenzeichen, was ein hohes 
Risiko mit sich bringt, dass der Domain-Name implizit dem Beschwerdeführer zugeordnet wird. 
[...] Unter Berücksichtigung dessen, dass der Beschwerdeführer Debian auf seiner Homepage 
prominent als ›Gemeinschaft‹ und nicht nur als ein Betriebssystem beschreibt, liegt die 
Annahme nahe, dass der [.community-] Suffix zu einer Website führt, die [von Debian] betrieben 
oder unterstützt wird. Der angefochtene Domain-Name enthält keine kritisierenden oder anderen 
Bestandteile, die dazu führen, diesen Eindruck zu vermeiden oder zu bestätigen.
</blockquote>

<p>
Außerdem stellte das Gremium fest:
</p>

<blockquote>
Die Belege, die der Beschwerdeführer eingereicht hat, zeigen, dass einige Einträge die DEBIAN-Marke 
mit Informationen über einen anrüchigen Sex-Kult, bekannte Sexualstraftäter und Versklavung von 
Frauen in Verbindung bringen, desweiteren enthält ein Beitrag Bilder einer Brandmarkierung, mutmaßlich 
im Genitalbereich des Opfers. Der Übergang zwischen den Informationen zum Beschwerdeführer zu dieser 
Art von Inhalten ist arrangiert und der Umfang der Inhalte ist nicht zufällig. Aus Sicht des Gremiums 
sind diese Beiträge absichtlich so angeordnet, dass sie einen Zusammenhang zwischen der DEBIAN-Marke und den 
anstößigen Vorgängen auf dieser Website implizieren und die Marke so schädigen.
</blockquote>

<p>
… und kam zu dem Schluss, dass:
</p>


<blockquote>
Nichts im Debian-Sozialvertrag oder sonst irgendwo deutet darauf hin, dass der Beschwerdeführer jemals 
dieser Art von Marken-Assoziationen zugestimmt hat, die der Antragsgegner auf seiner Website 
veröffentlicht. Der Antragsgegner weist zwar darauf hin, dass die DEBIAN-Marke lediglich im Bezug auf 
Software registriert ist. Die Beiträge greifen jedoch Mitglieder des Beschwerdeführers an, die 
DEBIAN-Software herstellen (statt der Software selbst), und nutzen die Marke in Verbindung mit dem 
strittigen Domain-Namen in einer Art und Weise, die absichtlich falsche Assoziationen mit der Marke 
selbst entstehen lässt.
</blockquote>

<p>
Debian ist darauf bedacht, dass seine Handelsmarken gemäß der 
<a href="$(HOME)/trademark">Handelsmarkenrichtlinie</a> verwendet werden 
und wird auch weiterhin Gegenschritte unternehmen, wenn die Richtlinie 
verletzt wird.
</p>

<p>
Der Inhalt von <q>debian.community</q> wurde jetzt mit einer 
<a href="$(HOME)/legal/debian-community-site">Seite ersetzt,</a>
welche die Situation erklärt und weitere Fragen beantwortet.
</p>

<p>
Die volle Entscheidung des WIPO ist (auf Englisch) nachzulesen unter:
<a href="https://www.wipo.int/amc/en/domains/search/case.jsp?case_id=58273">https://www.wipo.int/amc/en/domains/search/case.jsp?case_id=58273</a>


<h2>Über Debian</h2>

<p>Das Debian-Projekt ist ein Zusammenschluss von Entwicklern 
Freier Software, die ihre Zeit und Bemühungen einbringen, 
das vollständig freie Betriebssystem Debian zu erschaffen.</p>

<h2>Kontaktinformationen</h2>

<p>Weitere Informationen bekommen Sie auf der Debian-Website unter 
<a href="$(HOME)/">https://www.debian.org/</a> oder per Mail (auf Englisch) an:
&lt;press@debian.org&gt;.</p>
