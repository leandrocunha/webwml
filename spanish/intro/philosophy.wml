#use wml::debian::template title="Nuestra filosofía: por qué lo hacemos y cómo lo hacemos" MAINPAGE="true"

#include "$(ENGLISHDIR)/releases/info"
#use wml::debian::translation-check translation="6686348617abaf4b5672d3ef6eaab60d594cf86e"

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<div id="toc">
  <ul class="toc">
    <li><a href="#freedom">Nuestra misión: crear un sistema operativo libre</a></li>
    <li><a href="#how">Nuestros valores: cómo funciona la comunidad Debian</a></li>
  </ul>
</div>

<h2><a id="freedom">Nuestra misión: crear un sistema operativo libre</a></h2>

<p>El proyecto Debian es una asociación de personas que comparten un
objetivo: queremos crear un sistema operativo libre, disponible para
todo el mundo. Ahora bien, cuando utilizamos el término «libre» no estamos hablando de dinero,
sino que nos referimos a la <em>libertad</em> del software (N. del T.: en el original en inglés se utiliza el término «free», que es sinónimo de «gratuito» y de «libre»).</p>

<p style="text-align:center"><button type="button"><span class="fas fa-unlock-alt fa-2x"></span> <a href="free">Nuestra definición de software libre</a></button> <button type="button"><span class="fas fa-book-open fa-2x"></span> <a href="https://www.gnu.org/philosophy/free-sw">Lea la declaración de la Fundación de software libre</a></button></p>

<p>Quizá se pregunte por qué tanta gente decide dedicar tantas
horas de su tiempo a escribir software y empaquetarlo y mantenerlo
cuidadosamene para después regalarlo sin cobrar nada por él. Bueno,
hay muchas razones, estas son algunas:</p>

<ul>
  <li>A algunas personas, sencillamente, les gusta ayudar a los demás, y contribuir a un proyecto de software libre es una magnífica manera de hacerlo.</li>
  <li>Muchos desarrolladores y desarrolladoras escriben programas para aprender más acerca de las computadoras, de las diferentes arquitecturas y de los lenguajes de programación.</li>
  <li>Algunos desarrolladores contribuyen para decir «gracias» por todo el excelente software libre que han recibido de otros.</li>
  <li>Muchas personas en las instituciones académicas crean software libre para compartir el resultado de sus investigaciones.</li>
  <li>Las empresas también ayudan a mantener software libre para influir en el desarrollo de aplicaciones o para implementar funcionalidades nuevas rápidamente.</li>
  <li>Desde luego, ¡la mayoría de los desarrolladores de Debian participan porque les parece muy divertido!</li>
</ul>

<p>Aunque creemos en el software libre, respetamos el hecho de que algunas personas en ocasiones
tienen que instalar software no libre en sus máquinas – tanto si
quieren hacerlo como si no. Hemos decidido dar soporte a estos usuarios y usuarias siempre que sea
posible. Hay un número creciente de paquetes que instalan software
no libre en un sistema Debian.</p>

<aside>
<p><span class="fas fa-caret-right fa-3x"></span> Estamos comprometidos con el software libre y hemos formalizado este compromiso en un documento: nuestro <a href="$(HOME)/social_contract">Contrato social</a>.</p>
</aside>

<h2><a id="how">Nuestros valores: cómo funciona la comunidad</a></h2>

<p>El proyecto Debian tiene más de mil <a
href="people">desarrolladores y contribuidores</a> activos repartidos <a
href="$(DEVEL)/developers.loc">por todo el mundo</a>. Un proyecto de estas
dimensiones necesita una <a href="organization">estructura organizada</a> cuidadosamente.
Si se pregunta cómo funciona el proyecto Debian o si la comunidad
Debian tiene reglas y directrices, eche un vistazo a los documentos
siguientes:</p>

<div class="row">
  <!-- left column -->
  <div class="column column-left">
    <div style="text-align: left">
      <ul>
        <li><a href="$(DEVEL)/constitution">La Constitución de Debian</a>: <br>
          Describe la estructura organizativa y explica la forma en la que el proyecto Debian toma las decisiones formales.</li>
        <li><a href="../social_contract">El Contrato social y las Directrices de software libre</a>: <br>
          El Contrato social de Debian y las Directrices de software libre de Debian (DFSG, por sus siglas en inglés), como parte de dicho contrato, describen nuestro compromiso con el software libre y con la comunidad de software libre.</li>
        <li><a href="diversity">La Declaración de diversidad:</a> <br>
          El proyecto Debian da la bienvenida a todas las personas y las anima a participar sin importar cómo se identifiquen a sí mismas o la percepción que los demás tengan de ellas.</li>
      </ul>
    </div>
  </div>

<!-- right column -->
  <div class="column column-right">
    <div style="text-align: left">
      <ul>
        <li><a href="../code_of_conduct">El Código de conducta:</a> <br>
          Hemos adoptado un código de conducta para los y las participantes en nuestras listas de correo, canales IRC, etc.</li>
        <li><a href="../doc/developers-reference/">La Referencia del desarrollador:</a> <br>
          Este documento proporciona una visión general de los procedimientos recomendados y de los recursos disponibles para los desarrolladores y mantenedores de Debian.</li>
        <li><a href="../doc/debian-policy/">El manual de normas de Debian:</a> <br>
          Un manual que describe las normas requeridas por la distribución Debian, como la estructura y contenido del archivo de Debian, los requerimientos técnicos que todo paquete debe satisfacer para ser incluido, etc.</li>
      </ul>
    </div>
  </div>
</div>

<p style="text-align:center"><button type="button"><span class="fas fa-code fa-2x"></span> Debian por dentro: <a href="$(DEVEL)/">Rincón de los desarrolladores</a></button></p>
