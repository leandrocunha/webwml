#use wml::debian::translation-check translation="549ed0fce26a03a0a08c5aca5a6c51e4199c3e20"
<define-tag pagetitle>Encerramento da DebConf20 on-line</define-tag>

<define-tag release_date>2020-08-30</define-tag>
#use wml::debian::news

<p>
No sábado, 29 de agosto de 2020, a Conferência anual de Desenvolvedores(as) e
Contribuidores(as) Debian chegou ao fim.
</p>

<p>
A DebConf20 foi realizada de forma on-line pela primeira vez, por conta da
pandemia de coronavírus (COVID-19).
</p>

<p>
Todas as sessões foram transmitidas ao vivo, com diversas formas de
participação: via mensagem IRC, documentos colaborativos on-line e salas de
videoconferência.
</p>

<p>
Com mais de 850 participantes de 80 países diferentes, e um total de 100
palestras, sessões de discussão, "desconferências" Birds of a Feather (BoF) e
outras atividades, a
<a href="https://debconf20.debconf.org">DebConf20</a> foi um grande sucesso.
</p>

<p>
Quando ficou claro que a DebConf20 seria uma conferência exclusivamente
on-line, a equipe de vídeo da DebConf passou muito tempo nos meses seguintes
adaptando, melhorando e, em alguns casos, escrevendo do zero a tecnologia que
seria necessária para tornar uma DebConf on-line possível. Depois de lições
aprendidas na MiniDebConfOnline no final de maio, alguns ajustes foram
realizados e eventualmente chegamos a uma configuração envolvendo Jitsi, OBS,
Voctomix, SReview, nginx, Etherpad e interfaces web recém escritas para
voctomix, bem como para os diversos elementos da pilha.
</p>

<p>
Todos os componentes da infraestrutura de vídeo são software livre, e todo o
conjunto foi configurado pelo repositório público
<a href="https://salsa.debian.org/debconf-video-team/ansible">ansible</a>.
</p>

<p>
A <a href="https://debconf20.debconf.org/schedule/">agenda</a> da
DebConf20 incluiu duas trilhas em outras línguas que não o inglês: a MiniConf
em espanhol, com oito palestras em dois dias e a MiniConf em Malaio, com nove
palestras em três dias.
Atividade ad-hoc, introduzidas pelos participantes durante toda a
conferência, também foram possíveis, transmitidas e gravadas.
Também houve diversas reuniões de equipes para a realização de <a
href="https://wiki.debian.org/Sprints/">sprints</a> em certas áreas de
desenvolvimento do Debian.
</p>

<p>
Entre as palestras, a transmissão de vídeo exibiu os tradicionais
patrocinadores(as) em repetição, e também alguns clipes adicionais
incluindo fotos de Debconfs anteriores, curiosidades sobre o
Debian e vídeos curtos enviados pelos(as) participantes para se
comunicar com seus(suas) amigos(as) no Debian.
</p>

<p>
Para quem não pode participar, a maioria das palestras e
sessões já estão disponíveis através do
<a href="https://meetings-archive.debian.net/pub/debian-meetings/2020/DebConf20/">site web do arquivo de eventos do Debian</a>,
e os remanescentes serão disponibilizados nos próximos dias.
</p>

<p>
O site web <a href="https://debconf20.debconf.org/">DebConf20</a>
permanecerá ativo para fins de arquivamento e continuará a oferecer
links para as apresentações e vídeos das palestras e eventos.
</p>

<p>
No próximo ano, a <a
href="https://wiki.debian.org/DebConf/21">DebConf21</a>
está planejada para ocorrer em Haifa, Israel, em agosto ou setembro.
</p>

<p>
A DebConf está comprometida com um ambiente seguro e receptivo para
todos(as) os(as) participantes.
Durante a conferência, várias equipes (Recepção, equipe de boas-vindas
e equipe da Comunidade) estiveram disponíveis para ajudar os(as)
participantes a obter a melhor experiência na conferência e encontrar
soluções para qualquer problema que pudesse surgir. Consulte a
<a
href="https://debconf20.debconf.org/about/coc/">página web sobre o
código de conduta na página da DebConf20</a> para mais detalhes.
</p>

<p>
O Debian agradece o comprometimento de numerosos(as) <a
href="https://debconf20.debconf.org/sponsors/">patrocinadores(as)</a>
que apoiaram a DebConf20, em particular nossos(as)
patrocinadores(as) Platinum:
<a href="https://www.lenovo.com">Lenovo</a>,
<a href="https://www.infomaniak.com">Infomaniak</a>,
<a href="https://google.com/">Google</a>
e
<a href="https://aws.amazon.com/">Amazon Web Services (AWS)</a>.
</p>

<h2>Sobre o Debian</h2>
<p>
O Projeto Debian foi fundado em 1993 por Ian Murdock para ser um
verdadeiro projeto comunitário. Desde então o projeto cresceu para se
tornar um dos maiores e mais influentes projetos de código aberto.
Milhares de voluntários(as) de todo o mundo trabalham juntos(as) para criar e
manter software Debian. Disponível em 70 línguas, e oferecendo
suporte a uma enorme gama de tipos de computadores, o Debian
chama a si mesmo o <q>sistema operacional universal</q>.
</p>

<h2>Sobre a DebConf</h2>

<p>
A DebConf é a conferência de desenvolvedores(as) do Projeto Debian. Além
de um calendário repleto de palestras técnicas, sociais e políticas, a
DebConf oferece uma oportunidade para desenvolvedores(as), colaboradores(as) e
outras pessoas interessadas se encontrarem pessoalmente e trabalharem
conjuntamente e mais próximos(as). Ela acontece anualmente desde 2000 em locais
tão variados como Escócia, Argentina e Bósnia e Herzegóvina. Mais
informações sobre a DebConf estão disponíveis em
<a href="https://debconf.org/">https://debconf.org</a>.
</p>

<h2>Sobre a Lenovo</h2>

<p>
Enquanto líder global em tecnologia fabricando um amplo portfólio de
produtos conectados, incluindo smartphones, tablets, PCs e estações de
trabalho, bem como dispositivos de realidade virtual/aumentada,
soluções inteligentes para casa/escritório e datacenter, a <a
href="https://www.lenovo.com">Lenovo</a> entende como os sistemas e
plataformas abertas são críticos para um mundo conectado.
</p>

<h2>Sobre a Infomaniak</h2>

<p>
A <a href="https://www.infomaniak.com">Infomaniak</a> é a maior
empresa suíça de hospedagem web, oferecendo também serviços de backup
e armazenamento, soluções para organzação de eventos, transmissões
ao vivo e vídeo sobre demanda. É proprietária integral de seus
datacenters e de todos os elementos críticos para o funcionamento de
do serviços e produtos fornecidos pela empresa, tanto em software
como hardware.
</p>

<h2>Sobre o Google</h2>

<p>
O <a href="https://google.com/">Google</a> é uma das maiores empresas
de tecnologia no mundo, fornecendo uma ampla gama de serviços e
produtos relacionados à Internet, como tecnologias de publicidade
on-line, pesquisa, computação em nuvem, software e hardware.
</p>

<p>
O Google tem apoiado o Debian patrocinando a DebConf há mais de uma
década, e é também um parceiro Debian patrocinando parte da
infraestrutura de integração contínua da <a
href="https://salsa.debian.org">Salsa</a> dentro da
Plataforma Google Loudel.
</p>

<h2>Sobre Amazon Web Services (AWS)</h2>

<p>
<a href="https://aws.amazon.com">Amazon Web Services (AWS)</a> é uma
das plataformas de nuvem mais abrangentes e amplamente adotadas no
mundo, oferecendo mais de 175 serviços completos de datacenters
em todo o mundo em 77 zonas de disponibilidade dentro de 24
regiões geográficas. Os clientes da AWS incluem as startups de
crescimento mais rápido, as maiores empresas e as principais agências
governamentais.

<h2>Informações de Contato</h2>
<p>Para mais informações, por favor visite a página web da DebConf20
em
<a href="https://debconf20.debconf.org/">https://debconf20.debconf.org/</a>
ou envie um e-mail (em inglês) para
&lt;presa@debian.org&gt;.</p>

